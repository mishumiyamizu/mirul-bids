import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '../../services/token.service';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-login-external',
  templateUrl: './login-external.component.html',
  styleUrls: ['./login-external.component.scss']
})
export class LoginExternalComponent implements OnInit {

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public appService: AppService,
    public tokenService: TokenService) { }

  ngOnInit(): void {
    const accessToken = this.route.snapshot.queryParamMap.get('accessToken');
    const refreshToken = this.route.snapshot.queryParamMap.get('refreshToken');
    if (accessToken && refreshToken) {
      this.tokenService.setAccessToken(accessToken);
      this.tokenService.setRefreshToken(refreshToken);
      this.appService.loadLanguageFromToken();
      this.appService.loadThemeColorFromToken();
      this.appService.loadThemeModeFromToken();
      this.router.navigateByUrl('/');
    } else {
      this.router.navigateByUrl('404');
    }
  }

}
