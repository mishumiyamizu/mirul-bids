import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusRoute } from 'src/app/models/bus-route';
import { BusRouteEditorDialogComponent } from '../bus-route-editor-dialog/bus-route-editor-dialog.component';
import { BusRouteService } from 'src/app/services/bus-route.service';
import { BusRouteDeleteDialogComponent } from '../bus-route-delete-dialog/bus-route-delete-dialog.component';
import { strings as stringsZhCN } from "ngx-timeago/language-strings/zh-CN";
import { strings as stringsTh } from "ngx-timeago/language-strings/th";
import { strings as stringsEn } from "ngx-timeago/language-strings/en";
import { TimeagoIntl } from 'ngx-timeago';
import { TokenService } from 'src/app/services/token.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bus-route-detail',
  templateUrl: './bus-route-detail.component.html',
  styleUrls: ['./bus-route-detail.component.scss']
})
export class BusRouteDetailComponent implements OnInit {

  @Input() selectedBusRoute!: BusRoute;
  updatedBusRoute!: BusRoute;

  constructor(
    public dialog: MatDialog,
    public busRouteService: BusRouteService,
    public translate: TranslateService,
    public tokenService: TokenService,
    public intl: TimeagoIntl
  ) {
    this.updateLang(this.tokenService.getLanguage())

    this.translate.onLangChange.subscribe((res: any) => {
      this.updateLang(res.lang)
    })
   }

  ngOnInit(): void {
  }

  updateLang(lang: string) {
    if (lang == 'en') {
      this.intl.strings = stringsEn;
    }
    else if (lang == 'zh') {
      this.intl.strings = stringsZhCN;
    }
    else if (lang == 'th') {
      this.intl.strings = stringsTh;
    }
    else if (lang == 'ms') {
      this.intl.strings = {
        prefixAgo: '',
        prefixFromNow: '',
        suffixAgo: 'lepas',
        suffixFromNow: 'mula daripada sekarang',
        seconds: 'kurang dari 1 minit',
        minute: 'lebih kurang 1 minute',
        minutes: '%d minit',
        hour: 'lebih kurang 1 jam',
        hours: 'lebih kurang %d jam',
        day: '1 hari',
        days: '%d hari',
        month: 'lebih kurang 1 bulan',
        months: '%d bulan',
        year: 'lebih kurang 1 tahun',
        years: '%d tahun',
      }
    }
    else {
      this.intl.strings = stringsEn;
    }
    this.intl.changes.next();
  }

  openBusRouteEditDialog() {
    const dialogRef = this.dialog.open(BusRouteEditorDialogComponent, { disableClose: false, maxWidth: '60%', autoFocus: false, data: this.selectedBusRoute });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        const index = this.busRouteService.filteredBusRoutes.findIndex((busRoute) => busRoute.id === this.selectedBusRoute.id)
        if (index !== -1) {
          this.busRouteService.filteredBusRoutes[index] = res.data;
          this.busRouteService.filteredBusRoutes = [...this.busRouteService.filteredBusRoutes];

          this.updatedBusRoute = {
            ...res.data,
            originBusStop: JSON.parse(res.data.originBusStop),
            destBusStop: JSON.parse(res.data.destBusStop),
            directionToOrigin: JSON.parse(res.data.directionToOrigin),
            directionToDest: JSON.parse(res.data.directionToDest),
          }
          console.log(this.updatedBusRoute)

          this.selectedBusRoute = this.updatedBusRoute;
        }
      }
    })
  }

  openBusRouteDeleteDialog(){
    const dialogRef = this.dialog.open(BusRouteDeleteDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.selectedBusRoute });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.message == 'success') {
        const index = this.busRouteService.filteredBusRoutes.findIndex((bus) => bus.id === this.selectedBusRoute.id);
        this.busRouteService.filteredBusRoutes.splice(index, 1);
        this.selectedBusRoute = this.busRouteService.filteredBusRoutes[0];
      }
    })
  }

}
