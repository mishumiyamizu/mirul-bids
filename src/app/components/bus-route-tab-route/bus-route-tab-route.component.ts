import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusRoute, BusStopData } from 'src/app/models/bus-route';
import { BusRouteService } from 'src/app/services/bus-route.service';
import { TokenService } from 'src/app/services/token.service';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { CdkDragDrop, CdkDropList, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { BusStop } from 'src/app/models/bus-stop';

@Component({
  selector: 'app-bus-route-tab-route',
  templateUrl: './bus-route-tab-route.component.html',
  styleUrls: ['./bus-route-tab-route.component.scss']
})
export class BusRouteTabRouteComponent implements OnInit, OnChanges {

  @Input() selectedBusRoute!: any;
  @ViewChild('stopList', { static: true }) stopList!: CdkDropList;

  goDirection: BusStopData[] = [];
  backDirection: BusStopData[] = [];
  stopContainer: BusStopData[] = [];
  busRoutes: any;
  totalBusStop: number = 0;

  add: boolean = false;

  constructor(
    public busRouteService: BusRouteService,
    public busStopService: BusStopService,
    public dialog: MatDialog,
    public tokenService: TokenService,
  ) { }

  ngOnInit(): void {
    if (this.selectedBusRoute) {
      this.goDirection = this.selectedBusRoute.directionToDest;
      this.backDirection = this.selectedBusRoute.directionToOrigin;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

    this.busStopService.getBusStopList().then((res) => { // to get list of bus stop
      this.busStopService.processBusStopList(res);
      this.busStopService.busStops.forEach(x => {
        let data: BusStopData = {
          name: x.name,
          code: x.code,
          busStopSequence: 0,
          distance: 0,
          wdFirstBus: '',
          wdLastBus: '',
          satFirstBus: '',
          satLastBus: '',
          sunFirstBus: '',
          sunLastBus: ''
        }

        this.stopContainer.push(data)
      })
    })

    if (changes['selectedBusRoute'].firstChange == false) {
      this.goDirection = this.selectedBusRoute.directionToDest;
      this.backDirection = this.selectedBusRoute.directionToOrigin;
    }
  }

  dropBusStop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }

    this.goDirection.forEach((item, index) => {
      item.busStopSequence = index + 2; // to set sequence for each bus stop in direction to destination
      this.selectedBusRoute.destBusStop.busStopSequence = this.goDirection.length + 2; // to set sequence for destination bus stop
    })

    this.backDirection.forEach((item, index) => {
      item.busStopSequence = this.selectedBusRoute.destBusStop.busStopSequence + this.backDirection.length - index; // to set sequence for each bus stop in direction to origin
    })

    this.totalBusStop = this.goDirection.length + this.backDirection.length + 2;

    this.updateBusRoute()

  }

  updateBusRoute() {
    let updateBusRoute: BusRoute = {
      id: this.selectedBusRoute.id,
      tenantId: this.tokenService.getTenantId(),
      code: this.selectedBusRoute.code,
      name: this.selectedBusRoute.name,
      operatorCode: this.selectedBusRoute.operatorCode,
      totalBusStop: this.totalBusStop,
      originBusStop: this.selectedBusRoute.originBusStop,
      destBusStop: this.selectedBusRoute.destBusStop,
      directionToOrigin: this.backDirection,
      directionToDest: this.goDirection,
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
    }

    this.busRouteService.updateBusRoute(updateBusRoute)
      .then((res: any) => {
        console.log(res.data)
        res.data.destBusStop = JSON.parse(res.data.destBusStop);
        res.data.originBusStop = JSON.parse(res.data.originBusStop);
        res.data.directionToDest = JSON.parse(res.data.directionToDest);
        res.data.directionToOrigin = JSON.parse(res.data.directionToOrigin);

        const index = this.busRouteService.filteredBusRoutes.findIndex((busRoute) => busRoute.id === this.selectedBusRoute.id)
        if (index !== -1) {
          this.busRouteService.filteredBusRoutes[index] = res.data;
          this.busRouteService.filteredBusRoutes = [...this.busRouteService.filteredBusRoutes];
        }
      })
      .catch((err) => {
      })
      .finally(() => {
      })
  }
}
