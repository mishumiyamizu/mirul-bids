import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatDialog } from '@angular/material/dialog';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/services/app.service';
import { TokenService } from 'src/app/services/token.service';
import { Router, UrlSerializer } from '@angular/router';
import { Env } from 'src/app/models/env';

declare const env: Env;


interface Node {
  name: string;
  icon?: string;
  url?: string;
  children?: Node[];
}

@Component({
  selector: 'app-layout-sidebar',
  templateUrl: './layout-sidebar.component.html',
  styleUrls: ['./layout-sidebar.component.scss']
})
export class LayoutSidebarComponent implements OnInit {

  treeControl = new NestedTreeControl<Node>(node => node.children);
  dataSource = new MatTreeNestedDataSource<Node>();
  hasChild = (_: number, node: Node) => !!node.children && node.children.length > 0;

  title = env.applicationName;
  activeNode: any;

  verNo: string = env.versionNumber;

  constructor(
    public appService: AppService,
    public translate: TranslateService,
    public tokenService: TokenService,
    public router: Router,
    public urlSerializer: UrlSerializer,
    public dialog: MatDialog) {
    this.translate.use(this.appService.currentLanguage).subscribe(() => {
      this.dataSource.data = this.setupTreenode();
    });
    appService.onLanguageChanged.subscribe((language) => {
      this.dataSource.data = this.setupTreenode();
    });
  }

  ngOnInit(): void {
  }

  setupTreenode(): Node[] {
    let treeNodes: Node[] = [
      { name: this.translate.instant('GENERAL.BUS'), icon: 'directions_bus', url: './bus' },
      { name: this.translate.instant('GENERAL.BUS_STOP'), icon: 'report', url: './bus-stop' },
      { name: this.translate.instant('GENERAL.BUS_ROUTE'), icon: 'linear_scale', url: './bus-route' },
      { name: this.translate.instant('GENERAL.BUS_OPERATOR'), icon: 'person', url: './bus-operator' },
      { name: this.translate.instant('GENERAL.BUS_STOP_SCHEDULE'), icon: 'departure_board', url: './bus-stop-schedule' },
      { name: this.translate.instant('GENERAL.TERMINAL_BUS_SCHEDULE'), icon: 'schedule', url: './terminal-bus-schedule' }
    ];
    return treeNodes;
  }

  toggleSidebar() {
    this.appService.isSidebarOn = !this.appService.isSidebarOn;
  }

  setActiveNode(node: any) {
    console.log('node: ', node);
    this.activeNode = node;
    console.log('nodeURL: ', node.url.replace('./', ''));
    console.log('routerURL: ', this.router.url);
    console.log('true?: ', this.router.url.includes(node.url.replace('./', '')));
  }

  signOut() {
    this.appService.signOut();
  }

}
