import { Component, Input, OnInit } from '@angular/core';
import { BusStop } from 'src/app/models/bus-stop';
import { BusStopSchedule } from 'src/app/models/bus-stop-schedule';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-bus-stop-schedule-detail',
  templateUrl: './bus-stop-schedule-detail.component.html',
  styleUrls: ['./bus-stop-schedule-detail.component.scss']
})
export class BusStopScheduleDetailComponent implements OnInit {

  @Input() busStopSchedule!: BusStopSchedule;
  busStop!: any;
  busModule: number = 0;

  constructor(
    public busStopService: BusStopService,
    public fileUploadService: FileUploadService
  ) { }

  ngOnInit(): void {
    console.log(this.busStopSchedule.busStop)
    if (this.busStopSchedule.busStop == null) {
      console.log(this.busStopSchedule)
      this.busStopService.getBusStop(this.busStopSchedule.busStopId)
        .then((res) => {
          this.busStopSchedule.busStop = res
          this.fileUploadService.processImage(res); //important to get img, otherwise, it wont get img
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
        })
    }
  }

  busDetails(busNumber: number) {
    this.busModule = busNumber;
  }

}
