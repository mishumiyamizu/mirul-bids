import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Bus } from 'src/app/models/bus';
import { BusService } from 'src/app/services/bus.service';
import { Env } from 'src/app/models/env';
import { TokenService } from 'src/app/services/token.service';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { BusOperatorService } from 'src/app/services/bus-operator.service';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { BusRouteService } from 'src/app/services/bus-route.service';

declare const env: Env;

@Component({
  selector: 'app-bus-editor-dialog',
  templateUrl: './bus-editor-dialog.component.html',
  styleUrls: ['./bus-editor-dialog.component.scss']
})
export class BusEditorDialogComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  bus: Bus = new Bus();

  isSaving: boolean = false;
  title: string = '';

  @ViewChild('fileUpload') fileUpload: ElementRef | null = null;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusEditorDialogComponent>,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public busService: BusService,
    public busOperatorService: BusOperatorService,
    public busStopService: BusStopService,
    public busRouteService: BusRouteService,
    public fileUploadService: FileUploadService,
  ) {

    this.form = fb.group({
      code: [this.bus.code, { validators: [Validators.required] }],
      licensePlate: [this.bus.licensePlate, { validators: [Validators.required] }],
      operatorCode: [this.bus.operatorCode, { validators: [Validators.required] }],
      type: [this.bus.type, { validators: [Validators.required] }],
      wheelchair: [this.bus.wheelchair],
      direction: [this.bus.direction, { validators: [Validators.required] }],
      routeCode: [this.bus.routeCode, { validators: [Validators.required] }],
      originStopCode: [this.bus.originStopCode, { validators: [Validators.required] }],
      destStopCode: [this.bus.destStopCode, { validators: [Validators.required] }],
    })
  }

  ngOnInit(): void {
    if (this.data) {
      this.title = this.translate.instant('GENERAL.BUS_EDIT');
      this.bus = this.data;
      this.form.setValue({
        code: this.data.code,
        licensePlate: this.data.licensePlate,
        operatorCode: this.data.operatorCode,
        type: this.data.type,
        wheelchair: this.data.wheelchair,
        direction: this.data.direction.toString(),
        routeCode: this.data.routeCode,
        originStopCode: this.data.originStopCode,
        destStopCode: this.data.destStopCode,
      })

      if (this.data.avatar) {
        this.fileUploadService.imageSrc = new URL(this.data.avatar, env.bimsApiEndpoint).toString();
      }
      else
        this.fileUploadService.imageSrc = null;

    } else {
      this.title = this.translate.instant('GENERAL.BUS_CREATE');
      this.fileUploadService.imageSrc = null;
    }

    this.busOperatorService.getBusOperatorList().then((res) => { // to get list of bus operator
      this.busOperatorService.processBusOperatorList(res);
    })

    this.busStopService.getBusStopList().then((res) => { // to get list of bus stop
      this.busStopService.processBusStopList(res);
    })

    this.busRouteService.getBusRouteList().then((res) => { // to get list of bus route
      this.busRouteService.processBusRouteList(res);
    })
  }

  saveBus() {
    this.isSaving = true;
    let newBus: Bus = {
      id: '',
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      licensePlate: this.form.value.licensePlate,
      operatorCode: this.form.value.operatorCode,
      type: this.form.value.type,
      wheelchair: this.form.value.wheelchair,
      direction: this.form.value.direction,
      routeCode: this.form.value.routeCode,
      originStopCode: this.form.value.originStopCode,
      destStopCode: this.form.value.destStopCode,
      amPeakFreq: '',
      amOffpeakFreq: '',
      pmPeakFreq: '',
      pmOffpeakFreq: '',
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
      avatar: this.fileUploadService.file,
      imageUrl: '',
    }

    this.busService.createBus(newBus)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isSaving = false;
        this.fileUploadService.imageSrc = '';
        this.fileUploadService.file = null;
      })
  }

  updateBus() {
    this.isSaving = true;
    let updateBus: Bus = {
      id: this.data.id,
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      licensePlate: this.form.value.licensePlate,
      operatorCode: this.form.value.operatorCode,
      type: this.form.value.type,
      wheelchair: this.form.value.wheelchair,
      direction: this.form.value.direction,
      routeCode: this.form.value.routeCode,
      originStopCode: this.form.value.originStopCode,
      destStopCode: this.form.value.destStopCode,
      amPeakFreq: '',
      amOffpeakFreq: '',
      pmPeakFreq: '',
      pmOffpeakFreq: '',
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
      avatar: this.data.avatar,
      imageUrl: '',
    }

    this.busService.updateBus(updateBus)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.isSaving = false;
        this.fileUploadService.imageSrc = '';
        this.fileUploadService.file = null;
      })
  }

  confirm() {
    if (!this.data)
      this.saveBus()
    else
      this.updateBus()
  }

  avatarUpload(e: any) {
    this.fileUploadService.onFileSelected(e)

    if (this.fileUpload)
      this.fileUpload.nativeElement.value = '';
  }
}
