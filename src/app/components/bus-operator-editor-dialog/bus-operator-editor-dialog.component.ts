import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Env } from 'src/app/models/env';
import { TokenService } from 'src/app/services/token.service';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { BusOperator } from 'src/app/models/bus-operator';
import { BusOperatorService } from 'src/app/services/bus-operator.service';

declare const env: Env;

@Component({
  selector: 'app-bus-operator-editor-dialog',
  templateUrl: './bus-operator-editor-dialog.component.html',
  styleUrls: ['./bus-operator-editor-dialog.component.scss']
})
export class BusOperatorEditorDialogComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  busOperator: BusOperator = new BusOperator();

  isSaving: boolean = false;
  title: string = '';

  @ViewChild('fileUpload') fileUpload: ElementRef | null = null;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusOperatorEditorDialogComponent>,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public busOperatorService: BusOperatorService,
    public fileUploadService: FileUploadService,
  ) {

    this.form = fb.group({
      code: [this.busOperator.code, { validators: [Validators.required] }],
      name: [this.busOperator.name, { validators: [Validators.required] }],
      abbrName: [this.busOperator.abbrName, { validators: [Validators.required] }],
    })
  }

  ngOnInit(): void {
    if (this.data) {
      this.title = this.translate.instant('GENERAL.BUS_OPERATOR_EDIT');
      this.busOperator = this.data;
      this.form.setValue({
        code: this.data.code,
        name: this.data.name,
        abbrName: this.data.abbrName,
      })

      if (this.data.avatar) {
        this.fileUploadService.imageSrc = new URL(this.data.avatar, env.bimsApiEndpoint).toString();
      }
      else
        this.fileUploadService.imageSrc = null;

    } else {
      this.title = this.translate.instant('GENERAL.BUS_OPERATOR_CREATE');
      this.fileUploadService.imageSrc = null;
    }
  }

  saveBusOperator() {
    this.isSaving = true;
    let newBusOperator: BusOperator = {
      id: '',
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      name: this.form.value.name,
      abbrName: this.form.value.abbrName,
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
      avatar: this.fileUploadService.file,
      imageUrl: '',
    }

    this.busOperatorService.createBusOperator(newBusOperator)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isSaving = false;
        this.fileUploadService.imageSrc = '';
      })
  }

  updateBusOperator() {
    this.isSaving = true;
    let updateBusOperator: BusOperator = {
      id: this.data.id,
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      name: this.form.value.name,
      abbrName: this.form.value.abbrName,
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
      avatar: this.data.avatar,
      imageUrl: '',
    }

    this.busOperatorService.updateBusOperator(updateBusOperator)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isSaving = false;
        this.fileUploadService.imageSrc = '';
        this.fileUploadService.file = null;
      })
  }

  confirm() {
    if (!this.data)
      this.saveBusOperator()
    else
      this.updateBusOperator()
  }

  avatarUpload(e: any) {
    this.fileUploadService.onFileSelected(e)

    if (this.fileUpload)
      this.fileUpload.nativeElement.value = '';
  }

}
