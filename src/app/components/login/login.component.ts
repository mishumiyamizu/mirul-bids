import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { TokenService } from 'src/app/services/token.service';
import { Env } from 'src/app/models/env';

declare const env: Env;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  @ViewChild('myIframe', { static: true }) iframeRef!: ElementRef<HTMLIFrameElement>;

  constructor(
    private tokenService: TokenService,
    private appService: AppService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.initWindowEventListener();
  }

  ngAfterViewInit(): void {
    this.iframeRef.nativeElement.src = env.identityLoginUrl;
  }

  initWindowEventListener(){
    window.addEventListener('message', (event) => {
      if(event.data.msgCode === 'token'){
        let accessToken = event.data.token['accessToken'];
        let refreshToken = event.data.token['refreshToken'];
  
        this.tokenService.setAccessToken(accessToken);
        this.tokenService.setRefreshToken(refreshToken);
        this.appService.loadLanguageFromToken();
        this.appService.loadThemeColorFromToken();
        this.appService.loadThemeModeFromToken();
  
        this.router.navigate(['/']);
 
      }
    });
  }
}
