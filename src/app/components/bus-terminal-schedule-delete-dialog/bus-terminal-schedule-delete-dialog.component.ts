import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BusTerminalScheduleService } from 'src/app/services/bus-terminal-schedule.service';

@Component({
  selector: 'app-bus-terminal-schedule-delete-dialog',
  templateUrl: './bus-terminal-schedule-delete-dialog.component.html',
  styleUrls: ['./bus-terminal-schedule-delete-dialog.component.scss']
})
export class BusTerminalScheduleDeleteDialogComponent implements OnInit {

  isDeleting: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusTerminalScheduleDeleteDialogComponent>,
    public busTerminalService: BusTerminalScheduleService,
  ) { }

  ngOnInit(): void {
  }

  deleteBusTerminalSchedule() {
    this.isDeleting = true;
    this.busTerminalService.deleteBusTerminalSchedule(this.data.id)
      .then((res) => {
        this.dialogRef.close(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.isDeleting = false;
      })
  }
}
