import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusOperatorEditorDialogComponent } from '../bus-operator-editor-dialog/bus-operator-editor-dialog.component';
import { Env } from 'src/app/models/env';
import { BusOperator } from 'src/app/models/bus-operator';
import { BusOperatorService } from 'src/app/services/bus-operator.service';
import { BusOperatorDeleteDialogComponent } from '../bus-operator-delete-dialog/bus-operator-delete-dialog.component';
import { strings as stringsZhCN } from "ngx-timeago/language-strings/zh-CN";
import { strings as stringsTh } from "ngx-timeago/language-strings/th";
import { strings as stringsEn } from "ngx-timeago/language-strings/en";
import { TimeagoIntl } from 'ngx-timeago';
import { TokenService } from 'src/app/services/token.service';
import { TranslateService } from '@ngx-translate/core';

declare const env: Env;

@Component({
  selector: 'app-bus-operator-detail',
  templateUrl: './bus-operator-detail.component.html',
  styleUrls: ['./bus-operator-detail.component.scss']
})
export class BusOperatorDetailComponent implements OnInit {

  @Input() selectedBusOperator!: BusOperator;

  constructor(
    public dialog: MatDialog,
    public busOperatorService: BusOperatorService,
    public translate: TranslateService,
    public tokenService: TokenService,
    public intl: TimeagoIntl
  ) {
    this.updateLang(this.tokenService.getLanguage())

    this.translate.onLangChange.subscribe((res: any) => {
      this.updateLang(res.lang)
    })
   }

  ngOnInit(): void {
  }

  updateLang(lang: string) {
    if (lang == 'en') {
      this.intl.strings = stringsEn;
    }
    else if (lang == 'zh') {
      this.intl.strings = stringsZhCN;
    }
    else if (lang == 'th') {
      this.intl.strings = stringsTh;
    }
    else if (lang == 'ms') {
      this.intl.strings = {
        prefixAgo: '',
        prefixFromNow: '',
        suffixAgo: 'lepas',
        suffixFromNow: 'mula daripada sekarang',
        seconds: 'kurang dari 1 minit',
        minute: 'lebih kurang 1 minute',
        minutes: '%d minit',
        hour: 'lebih kurang 1 jam',
        hours: 'lebih kurang %d jam',
        day: '1 hari',
        days: '%d hari',
        month: 'lebih kurang 1 bulan',
        months: '%d bulan',
        year: 'lebih kurang 1 tahun',
        years: '%d tahun',
      }
    }
    else {
      this.intl.strings = stringsEn;
    }
    this.intl.changes.next();
  }

  openBusOperatorEditDialog() {
    const dialogRef = this.dialog.open(BusOperatorEditorDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.selectedBusOperator });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        const index = this.busOperatorService.busOperators.findIndex((busOp) => busOp.id === this.selectedBusOperator.id)
        if (index !== -1) {
          this.busOperatorService.filteredBusOperators[index] = res.data;
          this.busOperatorService.filteredBusOperators = [...this.busOperatorService.filteredBusOperators];
          this.selectedBusOperator = res.data;

          if(res.data.avatar)
            this.selectedBusOperator.imageUrl = new URL(res.data.avatar, env.bimsApiEndpoint).toString();
        }
      }
    })
  }

  openBusOperatorDeleteDialog() {
    const dialogRef = this.dialog.open(BusOperatorDeleteDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.selectedBusOperator });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.message == 'success') {
        const index = this.busOperatorService.filteredBusOperators.findIndex((busOp) => busOp.id === this.selectedBusOperator.id);
        this.busOperatorService.filteredBusOperators.splice(index, 1);
        this.selectedBusOperator = this.busOperatorService.filteredBusOperators[0];
      }
    })
  }
}
