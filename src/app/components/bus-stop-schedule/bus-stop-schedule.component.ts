import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { BusStopScheduleService } from 'src/app/services/bus-stop-schedule.service';
import { TokenService } from 'src/app/services/token.service';
import { debounceTime, switchMap } from 'rxjs';
import { BusStopService } from 'src/app/services/bus-stop.service';

@Component({
  selector: 'app-bus-stop-schedule',
  templateUrl: './bus-stop-schedule.component.html',
  styleUrls: ['./bus-stop-schedule.component.scss']
})
export class BusStopScheduleComponent implements OnInit, OnDestroy {

  constructor(
    public tokenService: TokenService,
    public busStopScheduleService: BusStopScheduleService,
    public busStopService: BusStopService,
  ) { }

  ngOnInit(): void {
    console.log(this.busStopScheduleService.busStopSchedules)
    if(this.busStopScheduleService.busStopSchedules.length == 0){
      this.busStopScheduleService.getBusStopScheduleList().then((res) => {
        this.busStopScheduleService.processBusList(res)
      })
  
      this.busStopScheduleService.search$.pipe(
        debounceTime(600),
      ).subscribe(() => {
        this.busStopScheduleService.filterBusStopSchedules();
        this.busStopScheduleService.isLoading = false;
      });
    }
  }

  ngOnDestroy(): void {
    this.busStopScheduleService.OnDestroy();
  }

  refresh() {
    this.ngOnInit();
  }

}
