import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BusStopService } from 'src/app/services/bus-stop.service';

@Component({
  selector: 'app-bus-stop-delete-dialog',
  templateUrl: './bus-stop-delete-dialog.component.html',
  styleUrls: ['./bus-stop-delete-dialog.component.scss']
})
export class BusStopDeleteDialogComponent implements OnInit {

  isDeleting: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusStopDeleteDialogComponent>,
    public busStopService: BusStopService,
  ) { }

  ngOnInit(): void {
  }

  deleteBusStop() {
    this.isDeleting = true;
    this.busStopService.deleteBusStop(this.data.id)
      .then((res) => {
        this.dialogRef.close(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.isDeleting = false;
      })
  }

}
