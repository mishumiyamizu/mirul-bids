import { Component, Input, OnInit } from '@angular/core';
import { BusTerminalScheduleDeleteDialogComponent } from '../bus-terminal-schedule-delete-dialog/bus-terminal-schedule-delete-dialog.component';
import { BusTerminalSchedule } from 'src/app/models/bus-terminal-schedule';
import moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { BusTerminalScheduleService } from 'src/app/services/bus-terminal-schedule.service';
import { BusTerminalScheduleEditorDialogComponent } from '../bus-terminal-schedule-editor-dialog/bus-terminal-schedule-editor-dialog.component';
import { BusTerminalScheduleInfoDialogComponent } from '../bus-terminal-schedule-info-dialog/bus-terminal-schedule-info-dialog.component';

@Component({
  selector: 'app-bus-terminal-schedule-detail',
  templateUrl: './bus-terminal-schedule-detail.component.html',
  styleUrls: ['./bus-terminal-schedule-detail.component.scss']
})
export class BusTerminalScheduleDetailComponent implements OnInit {
  @Input() busTerminalSchedule!: BusTerminalSchedule;

  constructor(
    public dialog: MatDialog,
    public busTerminalService: BusTerminalScheduleService
  ) {

  }

  ngOnInit(): void {
  }

  refresh() {
    this.busTerminalService.getBusTerminalScheduleList().then((res) => {
      this.busTerminalService.processBusTerminalScheduleList(res)
    })
  }

  openBusTerminalScheduleDeleteDialog() {
    const dialogRef = this.dialog.open(BusTerminalScheduleDeleteDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.busTerminalSchedule });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.message == 'success') {
        this.refresh()
      }
    })
  }

  openBusTerminalScheduleEditDialog() {
    const dialogRef = this.dialog.open(BusTerminalScheduleEditorDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.busTerminalSchedule });
    dialogRef.afterClosed().subscribe((res) => {
      this.refresh()
    })
  }

  openBusTerminalScheduleInfoDialog() {
    const dialogRef = this.dialog.open(BusTerminalScheduleInfoDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.busTerminalSchedule });
    dialogRef.afterClosed().subscribe((res) => {
    })
  }

  getDataDiff(startDate: any, endDate: any) {
    var startTime = moment(startDate);
    var endTime = moment(endDate);

    var duration = moment.duration(endTime.diff(startTime));
    var hours = Math.floor(duration.asHours()); // Get whole hours
    var minutes = duration.minutes(); // Get remaining minutes

    return hours + "h " + minutes + "m"
  }

}
