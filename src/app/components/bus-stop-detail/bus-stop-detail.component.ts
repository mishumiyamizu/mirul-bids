import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusStop } from 'src/app/models/bus-stop';
import { BusStopEditorDialogComponent } from '../bus-stop-editor-dialog/bus-stop-editor-dialog.component';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { BusStopDeleteDialogComponent } from '../bus-stop-delete-dialog/bus-stop-delete-dialog.component';
import { Env } from 'src/app/models/env';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { strings as stringsZhCN } from "ngx-timeago/language-strings/zh-CN";
import { strings as stringsTh } from "ngx-timeago/language-strings/th";
import { strings as stringsEn } from "ngx-timeago/language-strings/en";
import { TimeagoIntl } from 'ngx-timeago';
import { TokenService } from 'src/app/services/token.service';
import { TranslateService } from '@ngx-translate/core';

declare const env: Env;

@Component({
  selector: 'app-bus-stop-detail',
  templateUrl: './bus-stop-detail.component.html',
  styleUrls: ['./bus-stop-detail.component.scss']
})
export class BusStopDetailComponent implements OnInit, OnChanges {

  @Input() selectedBusStop!: BusStop;

  @ViewChild(MapInfoWindow, { static: false }) infoWindow!: MapInfoWindow;

  display: any;
  center: google.maps.LatLngLiteral = {
    lat: 0,
    lng: 0
  };
  zoom = 15;
  markerOptions: google.maps.MarkerOptions = { draggable: false, animation: google.maps.Animation.DROP };
  markerPositions: google.maps.LatLngLiteral[] = [];
  isSelected: boolean = true;

  constructor(
    public dialog: MatDialog,
    public busStopService: BusStopService,
    public translate: TranslateService,
    public tokenService: TokenService,
    public intl: TimeagoIntl
  ) {
    this.updateLang(this.tokenService.getLanguage())

    this.translate.onLangChange.subscribe((res: any) => {
      this.updateLang(res.lang)
    })
   }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.center = {
      lat: this.selectedBusStop.latitude,
      lng: this.selectedBusStop.longitude,
    }
  }

  updateLang(lang: string) {
    if (lang == 'en') {
      this.intl.strings = stringsEn;
    }
    else if (lang == 'zh') {
      this.intl.strings = stringsZhCN;
    }
    else if (lang == 'th') {
      this.intl.strings = stringsTh;
    }
    else if (lang == 'ms') {
      this.intl.strings = {
        prefixAgo: '',
        prefixFromNow: '',
        suffixAgo: 'lepas',
        suffixFromNow: 'mula daripada sekarang',
        seconds: 'kurang dari 1 minit',
        minute: 'lebih kurang 1 minute',
        minutes: '%d minit',
        hour: 'lebih kurang 1 jam',
        hours: 'lebih kurang %d jam',
        day: '1 hari',
        days: '%d hari',
        month: 'lebih kurang 1 bulan',
        months: '%d bulan',
        year: 'lebih kurang 1 tahun',
        years: '%d tahun',
      }
    }
    else {
      this.intl.strings = stringsEn;
    }
    this.intl.changes.next();
  }

  openBusStopEditDialog() {
    const dialogRef = this.dialog.open(BusStopEditorDialogComponent, { disableClose: false, width: '600px', autoFocus: false, data: this.selectedBusStop });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        const index = this.busStopService.busStops.findIndex((busStop) => busStop.id === this.selectedBusStop.id)
        if (index !== -1) {
          this.busStopService.filteredBusStops[index] = res.data;
          this.busStopService.filteredBusStops = [...this.busStopService.filteredBusStops];
          this.selectedBusStop = res.data;

          if (res.data.avatar)
            this.selectedBusStop.imageUrl = new URL(res.data.avatar, env.bimsApiEndpoint).toString();
        }
      }
    })
  }

  openBusStopDeleteDialog() {
    const dialogRef = this.dialog.open(BusStopDeleteDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.selectedBusStop });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.message == 'success') {
        const index = this.busStopService.filteredBusStops.findIndex((busStop) => busStop.id === this.selectedBusStop.id);
        this.busStopService.filteredBusStops.splice(index, 1);
        this.selectedBusStop = this.busStopService.filteredBusStops[0];
      }
    })
  }

  getStatusColor(status: string) {
    if (status === 'Open') {
      return '#33a532';
    } else if (status === 'Closed') {
      return '#cc3232';
    } else {
      return '#FF8C00';
    }
  }

  //? For tutorial google maps
  //* https://www.c-sharpcorner.com/article/how-to-integrate-google-maps-in-angular-14-app/


  moveMap(event: google.maps.MapMouseEvent) {
    if (event.latLng != null) this.center = (event.latLng.toJSON());
  }

  openInfoWindow(marker: MapMarker) {
    console.log(marker);
    this.infoWindow.open(marker);
  }

  move(event: google.maps.MapMouseEvent) {
    if (event.latLng != null) {
      this.display = event.latLng.toJSON();
    }
  }

  //? https://mapstyle.withgoogle.com/
  options: google.maps.MapOptions = {
    mapTypeId: 'roadmap',
    mapTypeControl: false,
    scrollwheel: true,
    maxZoom: 18,
    minZoom: 10,
    streetViewControl: false,
    fullscreenControl: false,
    styles: [
      {
        featureType: 'administrative',
        elementType: 'geometry',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
      {
        featureType: 'poi',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
      {
        featureType: 'road',
        elementType: 'labels.icon',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
      {
        featureType: 'transit',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
    ],
  };


}
