import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-bus-terminal-schedule-info-dialog',
  templateUrl: './bus-terminal-schedule-info-dialog.component.html',
  styleUrls: ['./bus-terminal-schedule-info-dialog.component.scss']
})
export class BusTerminalScheduleInfoDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
  }

}
