import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { BusRoute, BusStopData } from 'src/app/models/bus-route';
import { BusStop } from 'src/app/models/bus-stop';
import { BusOperatorService } from 'src/app/services/bus-operator.service';
import { BusRouteService } from 'src/app/services/bus-route.service';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-bus-route-editor-dialog',
  templateUrl: './bus-route-editor-dialog.component.html',
  styleUrls: ['./bus-route-editor-dialog.component.scss']
})
export class BusRouteEditorDialogComponent implements OnInit {

  leftDirection: BusStop[] = [];
  rightDirection: BusStop[] = [];
  direction: string = 'left'

  form: FormGroup = new FormGroup({});
  busRoute: BusRoute = new BusRoute();
  busRoutes: BusRoute[] = [];

  isSaving: boolean = false;
  title: string = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusRouteEditorDialogComponent>,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public busRouteService: BusRouteService,
    public busOperatorService: BusOperatorService,
    public busStopService: BusStopService,
  ) {
    this.form = fb.group({
      code: [this.busRoute.code, { validators: [Validators.required] }],
      name: [this.busRoute.name, { validators: [Validators.required] }],
      operatorCode: [this.busRoute.operatorCode, { validators: [Validators.required] }],
      originBusStopCode: ['', { validators: [Validators.required] }],
      destBusStopCode: ['', { validators: [Validators.required] }],
    })
  }

  ngOnInit(): void {
    console.log(this.data)
    if (this.data) {
      this.title = this.translate.instant('GENERAL.BUS_ROUTE_EDIT');
      this.busRoute = this.data
      this.form.setValue({
        code: this.data.code,
        name: this.data.name,
        operatorCode: this.data.operatorCode,
        originBusStopCode: this.data.originBusStop.code,
        destBusStopCode: this.data.destBusStop.code,

        // TODO add direction column for one way or loop service
        // direction: this.data.direction, 
      })

    } else {
      this.title = this.translate.instant('GENERAL.BUS_ROUTE_CREATE');
    }

    this.busOperatorService.getBusOperatorList().then((res) => { // to get list of bus operator
      this.busOperatorService.processBusOperatorList(res);
    })

    this.busStopService.getBusStopList().then((res) => { // to get list of bus stop
      this.busStopService.processBusStopList(res);
    })
  }

  createBusStopOrigin() {
    var tmpBusStop = this.busStopService.busStops.find(x => x.code === this.form.value.originBusStopCode)
    if (tmpBusStop) {
      this.busRoute.originBusStop = {
        code: tmpBusStop.code,
        name: tmpBusStop.name,
        busStopSequence: 1,
        distance: 0,
        wdFirstBus: '',
        wdLastBus: '',
        satFirstBus: '',
        satLastBus: '',
        sunFirstBus: '',
        sunLastBus: '',
      }
    }
    return this.busRoute.originBusStop;
  }

  createBusStopDest() {
    var tmpBusStop = this.busStopService.busStops.find(x => x.code === this.form.value.destBusStopCode)
    if (tmpBusStop) {
      this.busRoute.destBusStop = {
        code: tmpBusStop.code,
        name: tmpBusStop.name,
        busStopSequence: 2,
        distance: 0,
        wdFirstBus: '',
        wdLastBus: '',
        satFirstBus: '',
        satLastBus: '',
        sunFirstBus: '',
        sunLastBus: '',
      }
    }
    return this.busRoute.destBusStop;
  }

  saveBusRoute() {
    this.isSaving = true;

    let newBusRoute: BusRoute = {
      id: '',
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      name: this.form.value.name,
      operatorCode: this.form.value.operatorCode,
      totalBusStop: 2,
      originBusStop: this.createBusStopOrigin(),
      destBusStop: this.createBusStopDest(),
      directionToOrigin: [],
      directionToDest: [],
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
    }

    this.busRouteService.createBusRoute(newBusRoute)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isSaving = false;
      })
  }

  updateBusRoute() {
    this.isSaving = true;
    let updateBusRoute: BusRoute = {
      id: this.data.id,
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      name: this.form.value.name,
      operatorCode: this.form.value.operatorCode,
      totalBusStop: this.data.totalBusStop,
      originBusStop: this.createBusStopOrigin(),
      destBusStop: this.createBusStopDest(),
      directionToOrigin: this.data.directionToOrigin,
      directionToDest: this.data.directionToDest,
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
    }

    this.busRouteService.updateBusRoute(updateBusRoute)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.isSaving = false;
      })
  }

  confirm() {
    if (!this.data)
      this.saveBusRoute();
    else
      this.updateBusRoute();
  }
}
