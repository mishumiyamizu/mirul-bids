import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusRoute, BusRouteFilter } from 'src/app/models/bus-route';
import { TokenService } from 'src/app/services/token.service';
import { BusRouteService } from 'src/app/services/bus-route.service';
import { debounceTime, switchMap } from 'rxjs';
import { MatListOption } from '@angular/material/list';
import { MaterialService } from 'src/app/services/material.service';
import { BusRouteEditorDialogComponent } from '../bus-route-editor-dialog/bus-route-editor-dialog.component';

@Component({
  selector: 'app-bus-route-module',
  templateUrl: './bus-route-module.component.html',
  styleUrls: ['./bus-route-module.component.scss']
})
export class BusRouteModuleComponent implements OnInit, OnDestroy {

  filter!: BusRouteFilter
  selectedBusRoute!: BusRoute;
  busRoutes!: BusRoute[];

  @ViewChildren(MatListOption) matListOptions!: QueryList<MatListOption>;

  constructor(
    public dialog: MatDialog,
    public tokenService: TokenService,
    public busRouteService: BusRouteService,
    public matService: MaterialService,
  ) { }

  ngOnInit(): void {
    this.busRouteService.getBusRouteList().then((res) => {
      this.busRouteService.processBusRouteList(res)
      this.focusFirstItem();
      console.log(this.busRouteService.busRoutes)
    })

    this.busRouteService.search$.pipe(
      debounceTime(600),
    ).subscribe(() => {
      this.busRouteService.filterBusRoutes();
      this.focusFirstItem();
      this.busRouteService.isLoading = false;
      console.log(this.busRouteService.busRoutes)
    });
  }

  ngOnDestroy(): void {
    this.busRouteService.OnDestroy()
    this.matService.selectedItem = '';
  }

  handleSelectionChange(options: MatListOption[]) {
    this.selectedBusRoute = options[0].value;
  }

  focusFirstItem() {
    setTimeout(() => {
      this.matService.focusFirstItem(this.matListOptions);
      this.selectedBusRoute = this.matService.selectedItem;
    }, 50)
  }

  openNewBusRouteDialog() {
    const dialogRef = this.dialog.open(BusRouteEditorDialogComponent, { disableClose: false, maxWidth: '60%', autoFocus: false });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.ngOnInit();
      }
    })
  }

  resetKeyword() {
    this.busRouteService.filter.keyword = '';
    this.busRouteService.search();
  }

  refresh() {
    this.ngOnInit();
  }
}
