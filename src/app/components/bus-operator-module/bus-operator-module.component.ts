import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TokenService } from 'src/app/services/token.service';
import { debounceTime, switchMap } from 'rxjs';
import { MatListOption } from '@angular/material/list';
import { MaterialService } from 'src/app/services/material.service';
import { BusOperator, BusOperatorFilter } from 'src/app/models/bus-operator';
import { BusOperatorService } from 'src/app/services/bus-operator.service';
import { BusOperatorEditorDialogComponent } from '../bus-operator-editor-dialog/bus-operator-editor-dialog.component';

@Component({
  selector: 'app-bus-operator-module',
  templateUrl: './bus-operator-module.component.html',
  styleUrls: ['./bus-operator-module.component.scss']
})
export class BusOperatorModuleComponent implements OnInit, OnDestroy {

  filter!: BusOperatorFilter
  selectedBusOperator!: BusOperator;
  busOperators!: BusOperator[];

  @ViewChildren(MatListOption) matListOptions!: QueryList<MatListOption>;

  constructor(
    public dialog: MatDialog,
    public tokenService: TokenService,
    public busOperatorService: BusOperatorService,
    public matService: MaterialService,
  ) { }

  ngOnInit(): void {
    this.busOperatorService.getBusOperatorList().then((res) => {
      this.busOperatorService.processBusOperatorList(res)
      this.focusFirstItem();
      console.log(this.busOperatorService.busOperators)
    })

    this.busOperatorService.search$.pipe(
      debounceTime(600),
    ).subscribe(() => {
      this.busOperatorService.filterBusOperators();
      this.focusFirstItem();
      this.busOperatorService.isLoading = false;
      console.log(this.busOperatorService.busOperators)
    });
  }

  ngOnDestroy(): void {
    this.busOperatorService.OnDestroy()
    this.matService.selectedItem = '';
  }

  handleSelectionChange(options: MatListOption[]) {
    this.selectedBusOperator = options[0].value;
  }

  focusFirstItem() {
    setTimeout(() => {
      this.matService.focusFirstItem(this.matListOptions);
      this.selectedBusOperator = this.matService.selectedItem;
    }, 50)
  }

  openNewBusOperatorDialog() {
    const dialogRef = this.dialog.open(BusOperatorEditorDialogComponent, { disableClose: false, width: '480px', autoFocus: false });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.ngOnInit();
      }
    })
  }

  resetKeyword() {
    this.busOperatorService.filter.keyword = '';
    this.busOperatorService.search();
  }

  refresh() {
    this.busOperatorService.search();
    this.ngOnInit();
  }

}
