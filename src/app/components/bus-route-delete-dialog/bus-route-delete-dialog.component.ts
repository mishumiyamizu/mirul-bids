import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BusRouteService } from 'src/app/services/bus-route.service';

@Component({
  selector: 'app-bus-route-delete-dialog',
  templateUrl: './bus-route-delete-dialog.component.html',
  styleUrls: ['./bus-route-delete-dialog.component.scss']
})
export class BusRouteDeleteDialogComponent implements OnInit {

  isDeleting: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusRouteDeleteDialogComponent>,
    public busRouteService: BusRouteService,
  ) { }

  ngOnInit(): void {
  }

  deleteBusRoute() {
    this.isDeleting = true;
    this.busRouteService.deleteBusRoute(this.data.id)
      .then((res) => {
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isDeleting = false;
      })
  }

}
