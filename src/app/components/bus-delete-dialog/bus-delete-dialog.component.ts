import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BusService } from 'src/app/services/bus.service';

@Component({
  selector: 'app-bus-delete-dialog',
  templateUrl: './bus-delete-dialog.component.html',
  styleUrls: ['./bus-delete-dialog.component.scss']
})
export class BusDeleteDialogComponent implements OnInit {

  isDeleting: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusDeleteDialogComponent>,
    public busService: BusService,
  ) { }

  ngOnInit(): void {
  }

  deleteBus() {
    this.isDeleting = true;
    this.busService.deleteBus(this.data.id)
      .then((res) => {
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isDeleting = false;
      })
  }

}
