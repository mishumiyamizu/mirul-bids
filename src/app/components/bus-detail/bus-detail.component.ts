import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Bus } from 'src/app/models/bus';
import { BusEditorDialogComponent } from '../bus-editor-dialog/bus-editor-dialog.component';
import { BusService } from 'src/app/services/bus.service';
import { BusDeleteDialogComponent } from '../bus-delete-dialog/bus-delete-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { Env } from 'src/app/models/env';
import { strings as stringsZhCN } from "ngx-timeago/language-strings/zh-CN";
import { strings as stringsTh } from "ngx-timeago/language-strings/th";
import { strings as stringsEn } from "ngx-timeago/language-strings/en";
import { TimeagoIntl } from 'ngx-timeago';
import { TokenService } from 'src/app/services/token.service';

declare const env: Env;

@Component({
  selector: 'app-bus-detail',
  templateUrl: './bus-detail.component.html',
  styleUrls: ['./bus-detail.component.scss']
})
export class BusDetailComponent implements OnInit {

  @Input() selectedBus!: Bus;

  constructor(
    public dialog: MatDialog,
    public busService: BusService,
    public translate: TranslateService,
    public tokenService: TokenService,
    public intl: TimeagoIntl,
  ) {
    this.updateLang(this.tokenService.getLanguage());

    this.translate.onLangChange.subscribe((res: any) => {
      this.updateLang(res.lang)
    });
  }

  ngOnInit(): void {
  }

  updateLang(lang: string) {
    if (lang == 'en') {
      this.intl.strings = stringsEn;
    }
    else if (lang == 'zh') {
      this.intl.strings = stringsZhCN;
    }
    else if (lang == 'th') {
      this.intl.strings = stringsTh;
    }
    else if (lang == 'ms') {
      this.intl.strings = {
        prefixAgo: '',
        prefixFromNow: '',
        suffixAgo: 'lepas',
        suffixFromNow: 'mula daripada sekarang',
        seconds: 'kurang dari 1 minit',
        minute: 'lebih kurang 1 minute',
        minutes: '%d minit',
        hour: 'lebih kurang 1 jam',
        hours: 'lebih kurang %d jam',
        day: '1 hari',
        days: '%d hari',
        month: 'lebih kurang 1 bulan',
        months: '%d bulan',
        year: 'lebih kurang 1 tahun',
        years: '%d tahun',
      };
    }
    else {
      this.intl.strings = stringsEn;
    }
    this.intl.changes.next();
  }

  openBusEditDialog() {
    const dialogRef = this.dialog.open(BusEditorDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.selectedBus });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        const index = this.busService.buses.findIndex((bus) => bus.id === this.selectedBus.id)
        if (index !== -1) {
          this.busService.filteredBuses[index] = res.data;
          this.busService.filteredBuses = [...this.busService.filteredBuses];
          this.selectedBus = res.data;

          if (res.data.avatar)
            this.selectedBus.imageUrl = new URL(res.data.avatar, env.bimsApiEndpoint).toString();
        }
      }
    })
  }

  openBusDeleteDialog() {
    const dialogRef = this.dialog.open(BusDeleteDialogComponent, { disableClose: false, width: '480px', autoFocus: false, data: this.selectedBus });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.message == 'success') {
        const index = this.busService.filteredBuses.findIndex((bus) => bus.id === this.selectedBus.id);
        this.busService.filteredBuses.splice(index, 1);
        this.selectedBus = this.busService.filteredBuses[0];
      }
    })
  }

}
