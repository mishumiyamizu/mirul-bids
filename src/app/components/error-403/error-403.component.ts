import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-error-403',
  templateUrl: './error-403.component.html',
  styleUrls: ['./error-403.component.scss']
})
export class Error403Component implements OnInit {

  constructor(public location:Location) { }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

}
