import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BusOperatorService } from 'src/app/services/bus-operator.service';

@Component({
  selector: 'app-bus-operator-delete-dialog',
  templateUrl: './bus-operator-delete-dialog.component.html',
  styleUrls: ['./bus-operator-delete-dialog.component.scss']
})
export class BusOperatorDeleteDialogComponent implements OnInit {
  isDeleting: boolean = false; 

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusOperatorDeleteDialogComponent>,
    public busOperatorService: BusOperatorService,
  ) { }

  ngOnInit(): void {
  }

  deleteBus(){
    this.isDeleting = true;
    this.busOperatorService.deleteBusOperator(this.data.id)
    .then((res) => {
      this.dialogRef.close(res);
    })
    .catch((err) => {
    })
    .finally(() => {
      this.isDeleting = false;
    })
  }

}
