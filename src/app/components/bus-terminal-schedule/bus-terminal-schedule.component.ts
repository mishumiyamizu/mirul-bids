import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusTerminalScheduleService } from 'src/app/services/bus-terminal-schedule.service';
import { TokenService } from 'src/app/services/token.service';
import { BusTerminalScheduleEditorDialogComponent } from '../bus-terminal-schedule-editor-dialog/bus-terminal-schedule-editor-dialog.component';
import { debounceTime } from 'rxjs';


@Component({
  selector: 'app-bus-terminal-schedule',
  templateUrl: './bus-terminal-schedule.component.html',
  styleUrls: ['./bus-terminal-schedule.component.scss']
})
export class BusTerminalScheduleComponent implements OnInit {

  // selectedBusTerminal!: BusTerminalSchedule;

  fromDateSearch: string = ''
  toDateSearch: string = ''
  dateNow = new Date();

  constructor(
    public busTerminalService: BusTerminalScheduleService,
    public dialog: MatDialog,
    public tokenService: TokenService,
  ) {
  }

  ngOnInit(): void {
    if(this.busTerminalService.busTerminals.length == 0)
    this.busTerminalService.getBusTerminalScheduleList().then((res) => {
      this.busTerminalService.processBusTerminalScheduleList(res)
    })

    this.busTerminalService.search$.pipe(
      debounceTime(600),
    ).subscribe(() => {
      this.busTerminalService.filterBusTerminalSchedules();
      this.busTerminalService.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    this.busTerminalService.OnDestroy()
  }

  openNewBusTerminalScheduleDialog() {
    const dialogRef = this.dialog.open(BusTerminalScheduleEditorDialogComponent, { disableClose: false, width: '480px', autoFocus: false });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.busTerminalService.getBusTerminalScheduleList().then((res) => {
          this.busTerminalService.processBusTerminalScheduleList(res)
        })
      }
    })
  }

  refresh() {
    this.busTerminalService.getBusTerminalScheduleList().then((res) => {
      this.busTerminalService.processBusTerminalScheduleList(res)
    })
  }

  resetKeyword() {
    this.busTerminalService.filter.keyword = '';
    this.busTerminalService.search();
  }
}
