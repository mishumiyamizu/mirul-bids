import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from 'src/app/services/token.service';
import { AppService } from '../../services/app.service';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { Env } from 'src/app/models/env';
// import {BusDetailComponent} from 'src/app/components/bus-detail/bus-detail.component';

declare const env: Env;

@Component({
  selector: 'app-layout-main',
  templateUrl: './layout-main.component.html',
  styleUrls: ['./layout-main.component.scss']
})
export class LayoutMainComponent implements OnInit {
 
  title = env.applicationName;

  constructor(public appService: AppService,
    public translate: TranslateService,
    public tokenService: TokenService,
    public router: Router) {
  }

  ngOnInit(): void {
    this.init();
  }

  toggleSidebar() {
    this.appService.isSidebarOn = !this.appService.isSidebarOn;
  }

  init() {
    this.appService.getAvatar();
    this.appService.loadServiceComponentList();
    window.document.title = env.applicationName;
  }


  changeLanguage(lang: string) {
    this.translate.use(lang).subscribe(() => {
       this.appService.updateLanguage(lang);
      //  this.busDetailComponent.updateLang();
    });
  }

  changeTheme(theme: string) {
    this.appService.updateThemeColor(theme);
  }

  changeMode(mode: string) {
    this.appService.updateThemeMode(mode);
  }

  openLink(url: string, newTab: boolean, thirdParty: boolean) {
    if (!thirdParty) {
      let params = new HttpParams().append('accessToken', this.tokenService.accessTokenStr).append('refreshToken', this.tokenService.refreshTokenStr);
      url = decodeURIComponent(url + '?' + params.toString());
    }
    if (newTab)
      window.open(url, "_blank");
    else
      window.open(url, "_self");
  }

  signOut() {
    this.appService.signOut();
  }

  backToMain() {
    this.router.navigateByUrl('/main/bus');
  }
}
