import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { BusTerminalSchedule } from 'src/app/models/bus-terminal-schedule';
import { BusOperatorService } from 'src/app/services/bus-operator.service';
import { BusTerminalScheduleService } from 'src/app/services/bus-terminal-schedule.service';
import { TokenService } from 'src/app/services/token.service';
import { DateAdapter } from '@angular/material/core';
import * as moment from 'moment';

@Component({
  selector: 'app-bus-terminal-schedule-editor-dialog',
  templateUrl: './bus-terminal-schedule-editor-dialog.component.html',
  styleUrls: ['./bus-terminal-schedule-editor-dialog.component.scss']
})
export class BusTerminalScheduleEditorDialogComponent implements OnInit {
  form: FormGroup = new FormGroup({})
  busTerminalSchedule: BusTerminalSchedule = new BusTerminalSchedule();

  isSaving: boolean = false;
  title: string = '';

  minDate = moment().format('YYYY-MM-DD');

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dateAdapter: DateAdapter<Date>,
    public dialogRef: MatDialogRef<BusTerminalScheduleEditorDialogComponent>,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public busTerminalService: BusTerminalScheduleService,
    public busOperatorService: BusOperatorService,
  ) {
    this.dateAdapter.setLocale('en-GB'); 

    this.form = fb.group({
      origin: [this.busTerminalSchedule.origin, { validators: [Validators.required] }],
      destination: [this.busTerminalSchedule.destination, { validators: [Validators.required] }],
      tripCode: [this.busTerminalSchedule.tripCode, { validators: [Validators.required] }],
      gate: [this.busTerminalSchedule.gate, { validators: [Validators.required] }],
      bay: [this.busTerminalSchedule.bay, { validators: [Validators.required] }],
      operatorCode: [this.busTerminalSchedule.operatorCode, { validators: [Validators.required] }],
      plateNo: [this.busTerminalSchedule.plateNo, { validators: [Validators.required] }],
      date: [this.busTerminalSchedule.date, { validators: [Validators.required] }],
      departureTime: [this.busTerminalSchedule.departureTime, { validators: [Validators.required] }],
      arrivalTime: [this.busTerminalSchedule.arrivalTime, { validators: [Validators.required] }],
      type: [this.busTerminalSchedule.type, { validators: [Validators.required] }],
      totalSeat: [this.busTerminalSchedule.totalSeat, { validators: [Validators.required] }],
    })
  }

  ngOnInit(): void {
    if (this.data) {
      this.title = this.translate.instant('GENERAL.BUS_TERMINAL_SCHEDULE_EDIT');
      this.busTerminalSchedule = this.data;
      this.form.setValue({
        origin: this.data.origin,
        destination: this.data.destination,
        tripCode: this.data.tripCode,
        gate: this.data.gate,
        bay: this.data.bay,
        operatorCode: this.data.operatorCode,
        plateNo: this.data.plateNo,
        departureTime: moment(this.data.departureTime).format('HH:mm'),
        arrivalTime: moment(this.data.arrivalTime).format('HH:mm'),
        type: this.data.type,
        totalSeat: this.data.totalSeat,
        date: JSON.parse(this.data.date)
      })
    } else {
      this.title = this.translate.instant('GENERAL.BUS_TERMINAL_SCHEDULE_CREATE');
    }

    this.busOperatorService.getBusOperatorList().then((res) => { // to get list of bus operator
      this.busOperatorService.processBusOperatorList(res);
    })
  }

  saveBusTerminalSchedule() {
    this.isSaving = true;
    let newBusTerminal: BusTerminalSchedule = {
      id: '',
      tenantId: this.tokenService.getTenantId(),
      operatorCode: this.form.value.operatorCode,
      tripCode: this.form.value.tripCode,
      gate: this.form.value.gate,
      bay: this.form.value.bay,
      plateNo: this.form.value.plateNo,
      departureTime: this.form.value.departureTime,
      destination: this.form.value.destination,
      status: '',
      origin: this.form.value.origin,
      arrivalTime: this.form.value.arrivalTime,
      type: this.form.value.type,
      totalSeat: this.form.value.totalSeat,
      date: JSON.stringify(this.form.value.date)
    }

    this.busTerminalService.createBusTerminalSchedule(newBusTerminal)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isSaving = false;
      })
  }

  updateBusTerminalSchedule() {
    this.isSaving = true;
    let updateBusTermninal: BusTerminalSchedule = {
      id: this.data.id,
      tenantId: this.tokenService.getTenantId(),
      operatorCode: this.form.value.operatorCode,
      tripCode: this.form.value.tripCode,
      gate: this.form.value.gate,
      bay: this.form.value.bay,
      plateNo: this.form.value.plateNo,
      departureTime: this.form.value.departureTime,
      destination: this.form.value.destination,
      status: '',
      origin: this.form.value.origin,
      arrivalTime: this.form.value.arrivalTime,
      type: this.form.value.type,
      totalSeat: this.form.value.totalSeat,
      date: JSON.stringify(this.form.value.date)
    }

    this.busTerminalService.updateBusTerminalSchedule(updateBusTermninal)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.isSaving = false;
      })
  }

  confirm() {
    if (!this.data)
      this.saveBusTerminalSchedule()
    else
      this.updateBusTerminalSchedule()
  }

}
