import { Component, OnInit } from '@angular/core';
import { Env } from 'src/app/models/env';

declare const env: Env;

@Component({
  selector: 'app-layout-general',
  templateUrl: './layout-general.component.html',
  styleUrls: ['./layout-general.component.scss']
})
export class LayoutGeneralComponent implements OnInit {

  ngOnInit(): void {
    window.document.title = env.applicationName; 
  }

}
