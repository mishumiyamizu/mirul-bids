import { Component, OnInit, Inject, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { BusStop } from 'src/app/models/bus-stop';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { Env } from 'src/app/models/env';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { TokenService } from 'src/app/services/token.service';
import { combineLatest, forkJoin, map, merge } from 'rxjs';

declare const env: Env;

@Component({
  selector: 'app-bus-stop-editor-dialog',
  templateUrl: './bus-stop-editor-dialog.component.html',
  styleUrls: ['./bus-stop-editor-dialog.component.scss']
})
export class BusStopEditorDialogComponent implements OnInit, OnChanges {
  form: FormGroup = new FormGroup({});
  busStop: BusStop = new BusStop();

  isSaving: boolean = false;
  title: string = '';

  @ViewChild('fileUpload') fileUpload: ElementRef | null = null;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow!: MapInfoWindow;
  @ViewChild(MapMarker, { static: false }) marker!: MapMarker;

  display: any;
  center: google.maps.LatLngLiteral = {
    lat: 3.1342771247168257,
    lng: 101.68605635108257,
  };
  zoom = 13;
  markerOptions: google.maps.MarkerOptions = { draggable: true, animation: google.maps.Animation.DROP };
  markerPositions: google.maps.LatLngLiteral[] = [];

  timeZones: string[] = env.timezoneUtc;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusStopEditorDialogComponent>,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public busStopService: BusStopService,
    public fileUploadService: FileUploadService,
  ) {

    this.form = fb.group({
      code: [this.busStop.code, { validators: [Validators.required] }],
      name: [this.busStop.name, { validators: [Validators.required] }],
      latitude: [this.busStop.latitude, { validators: [Validators.required] }],
      longitude: [this.busStop.longitude, { validators: [Validators.required] }],
      roadName: [this.busStop.roadName, { validators: [Validators.required] }],
      landmark: [this.busStop.landmark],
      status: [this.busStop.status, { validators: [Validators.required] }],
      city: [this.busStop.city, { validators: [Validators.required] }],
      country: [this.busStop.country, { validators: [Validators.required] }],
      timezoneUTC: [this.busStop.timezoneUTC, { validators: [Validators.required] }],
    })

    this.form.get('latitude')?.valueChanges.subscribe((lat) => {
      const lng = this.form.get('longitude')?.value;

      this.center = {
        lat: lat,
        lng: lng
      }
    })

    this.form.get('longitude')?.valueChanges.subscribe((lng) => {
      const lat = this.form.get('latitude')?.value;

      this.center = {
        lat: lat,
        lng: lng
      }
    })
  }

  ngOnInit(): void {
    if (this.data) {
      this.title = this.translate.instant('GENERAL.BUS_STOP_EDIT');
      this.busStop = this.data
      this.form.setValue({
        code: this.data.code,
        name: this.data.name,
        latitude: this.data.latitude,
        longitude: this.data.longitude,
        roadName: this.data.roadName,
        landmark: this.data.landmark,
        status: this.data.status,
        city: this.data.city,
        country: this.data.country,
        timezoneUTC: this.data.timezoneUTC,
      })

      if (this.data.avatar) {
        this.fileUploadService.imageSrc = new URL(this.data.avatar, env.bimsApiEndpoint).toString();
      }
      else
        this.fileUploadService.imageSrc = null;

      this.center = {
        lat: this.data.latitude,
        lng: this.data.longitude
      }

    } else {
      this.title = this.translate.instant('GENERAL.BUS_STOP_CREATE');
      this.fileUploadService.imageSrc = null;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.center = {
      lat: this.form.value.latitude,
      lng: this.form.value.longitude
    };
  }

  saveBusStop() {
    this.isSaving = true;
    let newBusStop: BusStop = {
      id: '',
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      name: this.form.value.name,
      latitude: this.form.value.latitude,
      longitude: this.form.value.longitude,
      roadName: this.form.value.roadName,
      landmark: this.form.value.landmark,
      status: this.form.value.status,
      city: this.form.value.city,
      country: this.form.value.country,
      timezoneUTC: this.form.value.timezoneUTC,
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
      avatar: this.fileUploadService.file,
      imageUrl: '',
    }

    this.busStopService.createBusStop(newBusStop)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => {
      })
      .finally(() => {
        this.isSaving = false;
        this.fileUploadService.imageSrc = '';
      })
  }

  updateBusStop() {
    this.isSaving = true;
    let updateBusStop: BusStop = {
      id: this.data.id,
      tenantId: this.tokenService.getTenantId(),
      code: this.form.value.code,
      name: this.form.value.name,
      latitude: this.form.value.latitude,
      longitude: this.form.value.longitude,
      roadName: this.form.value.roadName,
      landmark: this.form.value.landmark,
      status: this.form.value.status,
      city: this.form.value.city,
      country: this.form.value.country,
      timezoneUTC: this.form.value.timezoneUTC,
      lastUpdate: '',
      updatedBy: this.tokenService.getEmail(),
      avatar: this.data.avatar,
      imageUrl: '',
    }

    this.busStopService.updateBusStop(updateBusStop)
      .then((res) => {
        this.form.markAsPristine();
        this.dialogRef.close(res);
      })
      .catch((err) => { 
      })
      .finally(() => {
        this.isSaving = false;
        this.fileUploadService.imageSrc = '';
        this.fileUploadService.file = null;
      })
  }

  confirm() {
    if (!this.data)
      this.saveBusStop();
    else
      this.updateBusStop();
  }

  avatarUpload(e: any) {
    this.fileUploadService.onFileSelected(e)

    if (this.fileUpload)
      this.fileUpload.nativeElement.value = '';
  }

  //? For tutorial google maps
  //* https://www.c-sharpcorner.com/article/how-to-integrate-google-maps-in-angular-14-app/

  openInfoWindow(marker: MapMarker) {
    this.infoWindow.open(marker);
  }

  move(event: google.maps.MapMouseEvent) {
    if (event.latLng != null) {
      this.display = event.latLng.toJSON();
    }
  }

  markerDragEnd(event: google.maps.MapMouseEvent) {
    if (event.latLng != null) {
      this.form.get('latitude')?.patchValue(event.latLng.toJSON().lat);
      this.form.get('longitude')?.patchValue(event.latLng.toJSON().lng);
    }
  }

  //? https://mapstyle.withgoogle.com/
  options: google.maps.MapOptions = {
    mapTypeId: 'roadmap',
    mapTypeControl: false,
    scrollwheel: true,
    maxZoom: 18,
    minZoom: 10,
    streetViewControl: false,
    fullscreenControl: false,
    styles: [
      {
        featureType: 'administrative',
        elementType: 'geometry',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
      {
        featureType: 'poi',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
      {
        featureType: 'road',
        elementType: 'labels.icon',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
      {
        featureType: 'transit',
        stylers: [
          {
            visibility: 'off',
          },
        ],
      },
    ],
  };

}
