import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusEditorDialogComponent } from '../bus-editor-dialog/bus-editor-dialog.component';
import { Bus, BusFilter } from 'src/app/models/bus';
import { TokenService } from 'src/app/services/token.service';
import { BusService } from 'src/app/services/bus.service';
import { debounceTime, switchMap } from 'rxjs';
import { MatListOption } from '@angular/material/list';
import { MaterialService } from 'src/app/services/material.service';

@Component({
  selector: 'app-bus-module',
  templateUrl: './bus-module.component.html',
  styleUrls: ['./bus-module.component.scss']
})
export class BusModuleComponent implements OnInit, OnDestroy {

  selectedBus!: Bus;

  @ViewChildren(MatListOption) matListOptions!: QueryList<MatListOption>;

  constructor(
    public dialog: MatDialog,
    public tokenService: TokenService,
    public busService: BusService,
    public matService: MaterialService,
  ) { }

  ngOnInit(): void {
    this.busService.getBusList().then((res) => {
      this.busService.processBusList(res)
      this.focusFirstItem();
      console.log(this.busService.buses)
    })

    this.busService.search$.pipe(
      debounceTime(600),
    ).subscribe(() => {
      this.busService.filterBuses();
      this.focusFirstItem();
      this.busService.isLoading = false;
      console.log(this.busService.buses)
    });
  }

  ngOnDestroy(): void {
    this.busService.OnDestroy()
    this.matService.selectedItem = '';
  }

  handleSelectionChange(options: MatListOption[]) {
    this.selectedBus = options[0].value;
  }

  focusFirstItem() {
    setTimeout(() => {
      this.matService.focusFirstItem(this.matListOptions);
      this.selectedBus = this.matService.selectedItem;
    }, 50)
  }

  openNewBusDialog() {
    const dialogRef = this.dialog.open(BusEditorDialogComponent, { disableClose: false, width: '480px', autoFocus: false });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.ngOnInit();
      }
    })
  }

  resetKeyword() {
    this.busService.filter.keyword = '';
    this.busService.search();
  }

  refresh() {
    this.ngOnInit();
  }
}
