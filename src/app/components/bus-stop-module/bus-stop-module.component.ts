import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatListOption } from '@angular/material/list';
import { BusStop } from 'src/app/models/bus-stop';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { MaterialService } from 'src/app/services/material.service';
import { TokenService } from 'src/app/services/token.service';
import { debounceTime, switchMap } from 'rxjs';
import { BusStopEditorDialogComponent } from '../bus-stop-editor-dialog/bus-stop-editor-dialog.component';

@Component({
  selector: 'app-bus-stop-module',
  templateUrl: './bus-stop-module.component.html',
  styleUrls: ['./bus-stop-module.component.scss']
})
export class BusStopModuleComponent implements OnInit, OnDestroy {
  selectedBusStop!: BusStop;

  @ViewChildren(MatListOption) matListOptions!: QueryList<MatListOption>;

  constructor(
    public dialog: MatDialog,
    public tokenService: TokenService,
    public busStopService: BusStopService,
    public matService: MaterialService,
  ) { }

  ngOnInit(): void {
    this.busStopService.getBusStopList().then((res) => {
      this.busStopService.processBusStopList(res)
      this.focusFirstItem();
      console.log(this.busStopService.busStops)
    })

    this.busStopService.search$.pipe(
      debounceTime(600),
    ).subscribe(() => {
      this.busStopService.filterBusStops();
      this.focusFirstItem();
      this.busStopService.isLoading = false;
      console.log(this.busStopService.busStops)
    });
  }

  ngOnDestroy(): void {
    this.busStopService.OnDestroy()
    this.matService.selectedItem = '';
  }

  handleSelectionChange(options: MatListOption[]) {
    this.selectedBusStop = options[0].value;
  }

  focusFirstItem() {
    setTimeout(() => {
      this.matService.focusFirstItem(this.matListOptions);
      this.selectedBusStop = this.matService.selectedItem;
    }, 50)
  }

  openNewBusStopDialog() {
    const dialogRef = this.dialog.open(BusStopEditorDialogComponent, { disableClose: false, width: '600px', autoFocus: false });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        //TODO to refresh bus-stop-schedule
        this.ngOnInit();
      }
    })
  }

  resetKeyword() {
    this.busStopService.filter.keyword = '';
    this.busStopService.search();
  }

  refresh() {
    this.ngOnInit();
  }
}
