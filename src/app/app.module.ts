import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatGridListModule } from '@angular/material/grid-list';
import { BoldPipe } from './pipes/bold.pipe';
import { ReplacePipe } from './pipes/replace.pipe';
import { Replace2Pipe } from './pipes/replace2.pipe';
import { DateTimePipe } from './pipes/dateTime.pipe';
import { LowercasePipe } from './pipes/lowercase.pipe';
import { Utc2LocalPipe } from './pipes/utc2LocalTime.pipe';
import { TimeagoCustomFormatter, TimeagoFormatter, TimeagoIntl, TimeagoModule } from 'ngx-timeago';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { LOCATION_INITIALIZED } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipDefaultOptions, MatTooltipModule, MAT_TOOLTIP_DEFAULT_OPTIONS } from '@angular/material/tooltip';
import { CustomHttpInterceptor } from './interceptors/custom-http.interceptor';
import { TokenService } from './services/token.service';
import { MdModule } from './md.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { GoogleMapsModule } from '@angular/google-maps';
//components
import { Error403Component } from './components/error-403/error-403.component';
import { Error404Component } from './components/error-404/error-404.component';
import { Error500Component } from './components/error-500/error-500.component';
import { ErrorE403DialogComponent } from './components/error-e403-dialog/error-e403-dialog.component';
import { LayoutGeneralComponent } from './components/layout-general/layout-general.component';
import { LayoutMainComponent } from './components/layout-main/layout-main.component';
import { LayoutSidebarComponent } from './components/layout-sidebar/layout-sidebar.component';
import { BusModuleComponent } from './components/bus-module/bus-module.component';
import { BusStopModuleComponent } from './components/bus-stop-module/bus-stop-module.component';
import { BusRouteModuleComponent } from './components/bus-route-module/bus-route-module.component';
import { BusOperatorModuleComponent } from './components/bus-operator-module/bus-operator-module.component';
import { BusStopScheduleComponent } from './components/bus-stop-schedule/bus-stop-schedule.component';
import { BusEditorDialogComponent } from './components/bus-editor-dialog/bus-editor-dialog.component';
import { BusDetailComponent } from './components/bus-detail/bus-detail.component';
import { BusDeleteDialogComponent } from './components/bus-delete-dialog/bus-delete-dialog.component';
import { BusStopEditorDialogComponent } from './components/bus-stop-editor-dialog/bus-stop-editor-dialog.component';
import { BusStopDetailComponent } from './components/bus-stop-detail/bus-stop-detail.component';
import { BusStopDeleteDialogComponent } from './components/bus-stop-delete-dialog/bus-stop-delete-dialog.component';
import { BusRouteEditorDialogComponent } from './components/bus-route-editor-dialog/bus-route-editor-dialog.component';
import { BusRouteDetailComponent } from './components/bus-route-detail/bus-route-detail.component';
import { BusRouteTabRouteComponent } from './components/bus-route-tab-route/bus-route-tab-route.component';
import { BusRouteDeleteDialogComponent } from './components/bus-route-delete-dialog/bus-route-delete-dialog.component';
import { BusOperatorEditorDialogComponent } from './components/bus-operator-editor-dialog/bus-operator-editor-dialog.component';
import { BusOperatorDetailComponent } from './components/bus-operator-detail/bus-operator-detail.component';
import { BusOperatorDeleteDialogComponent } from './components/bus-operator-delete-dialog/bus-operator-delete-dialog.component';
import { BusTerminalScheduleComponent } from './components/bus-terminal-schedule/bus-terminal-schedule.component';
import { BusStopScheduleDetailComponent } from './components/bus-stop-schedule-detail/bus-stop-schedule-detail.component';
import { BusTerminalScheduleEditorDialogComponent } from './components/bus-terminal-schedule-editor-dialog/bus-terminal-schedule-editor-dialog.component';
import { BusTerminalScheduleDeleteDialogComponent } from './components/bus-terminal-schedule-delete-dialog/bus-terminal-schedule-delete-dialog.component';
import { BusTerminalScheduleDetailComponent } from './components/bus-terminal-schedule-detail/bus-terminal-schedule-detail.component';
import { BusTerminalScheduleInfoDialogComponent } from './components/bus-terminal-schedule-info-dialog/bus-terminal-schedule-info-dialog.component';

export const tooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 100,
  hideDelay: 0,
  touchendHideDelay: 0,
  disableTooltipInteractivity: true
};

@NgModule({
  declarations: [
    AppComponent,
    BoldPipe,
    ReplacePipe,
    Replace2Pipe,
    DateTimePipe,
    LowercasePipe,
    Utc2LocalPipe,
    Error403Component,
    Error404Component,
    Error500Component,
    ErrorE403DialogComponent,
    LayoutGeneralComponent,
    LayoutMainComponent,
    LayoutSidebarComponent,
    BusModuleComponent,
    BusStopModuleComponent,
    BusRouteModuleComponent,
    BusOperatorModuleComponent,
    BusStopScheduleComponent,
    BusEditorDialogComponent,
    BusDetailComponent,
    BusDeleteDialogComponent,
    BusStopEditorDialogComponent,
    BusStopDetailComponent,
    BusStopDeleteDialogComponent,
    BusRouteEditorDialogComponent,
    BusRouteDeleteDialogComponent,
    BusOperatorEditorDialogComponent,
    BusOperatorDetailComponent,
    BusOperatorDeleteDialogComponent,
    BusRouteDetailComponent,
    BusRouteTabRouteComponent,
    BusTerminalScheduleComponent,
    BusStopScheduleDetailComponent,
    BusTerminalScheduleEditorDialogComponent,
    BusTerminalScheduleDeleteDialogComponent,
    BusTerminalScheduleDetailComponent,
    BusTerminalScheduleInfoDialogComponent
  ],
  imports: [
    BrowserModule,
    MatGridListModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTooltipModule,
    TimeagoModule.forRoot({
      formatter: {
        provide: TimeagoFormatter,
        useClass: TimeagoCustomFormatter
      },
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'en', //default language set to english
    }),
    FormsModule,
    ReactiveFormsModule,
    MdModule,
    DragDropModule,
    GoogleMapsModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [TokenService],
      useFactory: (tokenService: TokenService) => () => {
        tokenService.loadAccessToken();
        tokenService.loadRefreshToken();
      }
    },
    {
      provide: APP_INITIALIZER,
      useFactory: TranslateFactory,
      deps: [TranslateService, Injector],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true
    },
    { 
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS, 
      useValue: tooltipDefaults 
    },
    {
      provide: TimeagoIntl
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }

//#region translation

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function TranslateFactory(translate: TranslateService, injector: Injector) {
  return async () => {
    await injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
    const defaultLang = 'en';
    translate.addLangs(['en', 'ms', 'zh']);
    translate.setDefaultLang(defaultLang);
    try {
      await translate.use(defaultLang).toPromise();
    } catch (err) {
      console.log(err);
    }
  };
}

//#endregion