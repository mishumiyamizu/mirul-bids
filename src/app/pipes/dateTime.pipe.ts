import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateTime'
})
export class DateTimePipe implements PipeTransform {
  transform(value: string, arg: string = 'full'): any {
    if (value) {
      switch (arg) {
        case 'full':
          return moment(value).format('ddd, DD/MM/YYYY HH:mm:ss');
        case 'date-time':
          return moment(value).format('DD/MM/YYYY HH:mm:ss');
        case 'date-time-2':
          return moment(value).format('DD/MM/YYYY HH:mm');
        case 'time':
          return moment(value).format('HH:mm:ss');
        case 'time-2':
          return moment(value).format('HH:mm');
        case 'date':
          return moment(value).format('DD/MM/YYYY');
        default:
          return moment(value).format(arg);
      }
    } else {
      return value;
    }
  }
}
