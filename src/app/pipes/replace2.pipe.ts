import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace2'
})
export class Replace2Pipe implements PipeTransform {
  transform(value: string, search: string, replacement: string): string {
    if (!value || !search) {
      return value;
    }
    return value.replace(new RegExp(search, 'g'), replacement);
  }
}
