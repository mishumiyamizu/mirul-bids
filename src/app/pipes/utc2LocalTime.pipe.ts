import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment-timezone';
import { TokenService } from '../services/token.service';

@Pipe({
    name: 'utc2LocalTime'
})
export class Utc2LocalPipe implements PipeTransform {

    constructor(private tokenService: TokenService) { }

    transform(value: string, format: string = 'YYYY/MM/DD HH:mm:ss'): string {
        let timeZone = this.tokenService.getTimeZone();
        let time = moment.utc(value).tz(timeZone).format(format);
        return time;
    }
}