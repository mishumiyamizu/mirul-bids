import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutGeneralComponent } from './components/layout-general/layout-general.component';
import { LoginExternalComponent } from './components/login-external/login-external.component';
import { LoginComponent } from './components/login/login.component';
import { Error403Component } from './components/error-403/error-403.component';
import { Error404Component } from './components/error-404/error-404.component';
import { Error500Component } from './components/error-500/error-500.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { LayoutMainComponent } from './components/layout-main/layout-main.component';
import { LayoutSidebarComponent } from './components/layout-sidebar/layout-sidebar.component';
import { BusModuleComponent } from './components/bus-module/bus-module.component';
import { BusStopModuleComponent } from './components/bus-stop-module/bus-stop-module.component';
import { BusRouteModuleComponent } from './components/bus-route-module/bus-route-module.component';
import { BusOperatorModuleComponent } from './components/bus-operator-module/bus-operator-module.component';
import { BusStopScheduleComponent } from './components/bus-stop-schedule/bus-stop-schedule.component';
import { BusTerminalScheduleComponent } from './components/bus-terminal-schedule/bus-terminal-schedule.component';

const routes: Routes = [
  {
    path: '', component: LayoutGeneralComponent, children: [
      { path: '', redirectTo: '/main/bus', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'login-external', component: LoginExternalComponent },
      { path: '403', component: Error403Component },
      { path: '500', component: Error500Component },
      { path: '404', component: Error404Component }
    ]
  },
  {
    path: 'main', component: LayoutMainComponent, canActivate: [AuthGuardService], children: [
      {
        path: '', component: LayoutSidebarComponent, children: [
          { path: 'bus', component: BusModuleComponent },
          { path: 'bus-stop', component: BusStopModuleComponent },
          { path: 'bus-route', component: BusRouteModuleComponent },
          { path: 'bus-operator', component: BusOperatorModuleComponent },
          { path: 'bus-stop-schedule', component: BusStopScheduleComponent },
          { path: 'terminal-bus-schedule', component: BusTerminalScheduleComponent },
          { path: '403', component: Error403Component },
          { path: '500', component: Error500Component },
          { path: '404', component: Error404Component }
        ]
      }
    ]
  },
  { path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
