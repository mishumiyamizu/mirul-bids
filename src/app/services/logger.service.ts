import { Injectable, isDevMode } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })
export class LoggerService {

    constructor(){}

    debug(message: any) {
        if (isDevMode()) {
            console.debug(message);
        }
    }
}