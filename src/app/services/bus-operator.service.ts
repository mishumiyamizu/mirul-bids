import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/app/models/env';
import { Result } from 'src/app/models/result';
import { Observable, Subject, delay, retryWhen } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from './token.service';
import { FileUploadService } from './file-upload.service';
import { BusOperator, BusOperatorFilter } from '../models/bus-operator';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class BusOperatorService {
  URL_BUS_OPERATOR_CREATE = '/api/bus-operator/create';
  URL_BUS_OPERATOR_UPDATE = '/api/bus-operator/update';
  URL_BUS_OPERATOR_DELETE = '/api/bus-operator/delete';
  URL_BUS_OPERATOR_GET = '/api/bus-operator/item';
  URL_BUS_OPERATOR_LIST_GET = '/api/bus-operator/list';

  filter: BusOperatorFilter = {
    pageIndex: 0,
    pageSize: 20,
    sortBy: 'code',
    ascSort: true,
    keyword: '',
    tenantId: this.tokenService.getTenantId()
  }

  isLoading: boolean = false;
  isEmpty: boolean = false;
  busOperators: BusOperator[] = [];
  filteredBusOperators: BusOperator[] = [];

  search$: Subject<void> = new Subject<void>();
  cancel$: Subject<void> = new Subject<void>();

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public fileUploadService: FileUploadService,
  ) { }

  //#region create bus operator
  createBusOperator(busOperator: BusOperator): Promise<BusOperator> {
    return new Promise<BusOperator>((resolve, reject) => {
      const url = new URL(this.URL_BUS_OPERATOR_CREATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busOperator.id);
      formData.append('tenantId', busOperator.tenantId);
      formData.append('code', busOperator.code);
      formData.append('name', busOperator.name);
      formData.append('abbrName', busOperator.abbrName);
      formData.append('updatedBy', busOperator.updatedBy);

      if (this.fileUploadService.file)
        formData.append('img', this.fileUploadService.file); 

      this.http.post<BusOperator>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region update bus operator
  updateBusOperator(busOperator: BusOperator): Promise<BusOperator> {
    return new Promise<BusOperator>((resolve, reject) => {
      const url = new URL(this.URL_BUS_OPERATOR_UPDATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      if (!this.fileUploadService.imageSrc)
        busOperator.avatar = '';

      formData.append('id', busOperator.id);
      formData.append('tenantId', busOperator.tenantId);
      formData.append('code', busOperator.code);
      formData.append('name', busOperator.name);
      formData.append('abbrName', busOperator.abbrName);
      formData.append('updatedBy', busOperator.updatedBy);
      formData.append('avatar', busOperator.avatar);

      if (this.fileUploadService.file)
        formData.append('img', this.fileUploadService.file);

      this.http.post<BusOperator>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus operator list 
  getBusOperatorList(): Promise<BusOperator[]> {
    this.cancel$.next();

    return new Promise<BusOperator[]>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_OPERATOR_LIST_GET, env.bimsApiEndpoint).toString();

      this.http.post<BusOperator[]>(url, this.filter)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.cancel$.complete();
            this.isLoading = false;

          },
          error: (err) => {
            reject(new Error(err.message));
            this.cancel$.complete();
            this.isLoading = false;
          },
        });
    })
  }

  processBusOperatorList(res: any) {
    this.busOperators = res.data;
    this.filterBusOperators();
    this.fileUploadService.processImages(res.data); //important to get img, otherwise, it wont get img

    if (this.busOperators.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }

  OnDestroy() {
    this.cancel$.next();
    this.cancel$.complete();
    this.filter.keyword = '';
  }

  search() {
    this.search$.next();
    this.isLoading = true;
  }

  filterBusOperators() {
    this.filteredBusOperators = this.busOperators.filter(x =>
      x.code.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.name.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.abbrName.toLowerCase().includes(this.filter.keyword.toLowerCase())
    );

    if (this.filteredBusOperators.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }
  //#endregion

  //#region delete bus operator
  deleteBusOperator(busOperatorId: string): Promise<Result> {
    return new Promise<Result>((resolve, reject) => {
      const url = new URL(this.URL_BUS_OPERATOR_DELETE, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', busOperatorId)

      this.http.delete<any>(url, { params: params })
        .subscribe({
          next: (res) => {
            res.message = 'success'
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion
}
