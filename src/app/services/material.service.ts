import { Injectable, QueryList } from '@angular/core';
import { MatListOption } from '@angular/material/list';
import { Bus } from '../models/bus';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  selectedItem!: any

  constructor() { }

  //focus first mat-list-item
  focusFirstItem(matListOptions: QueryList<MatListOption>) {
    if (matListOptions && matListOptions.length > 0) {
      const firstList: MatListOption = matListOptions.first;
      this.selectedItem = firstList.value;
      firstList.selected = true;
    }
  }
}
