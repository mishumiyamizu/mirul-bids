import { Injectable } from '@angular/core';
import { Env } from 'src/app/models/env';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  file: any;
  imageSrc: any;

  onFileSelected(event: any) {
    this.file = event.target.files[0];
    if (this.file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        this.imageSrc = reader.result;
      }
      reader.readAsDataURL(this.file);
    }
  }

  removeImage() {
    this.imageSrc = null;
    this.file = null;
  }

  processImages(data: any[]) {
    data.forEach(x => {
      if (x.avatar)
        x.imageUrl = new URL(x.avatar, env.bimsApiEndpoint).toString();
    });
  }

  processImage(data: any) {
    if (data.avatar)
      data.imageUrl = new URL(data.avatar, env.bimsApiEndpoint).toString();
  }
}
