import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/app/models/env';
import { Result } from 'src/app/models/result';
import { Observable, Subject, delay, retryWhen } from 'rxjs';
import { BusStopSchedule, BusStopScheduleFilter } from '../models/bus-stop-schedule';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from './token.service';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class BusStopScheduleService {
  URL_BUS_STOP_SCHEDULE_CREATE = '/api/bus-stop-schedule/create';
  URL_BUS_STOP_SCHEDULE_UPDATE = '/api/bus-stop-schedule/update';
  URL_BUS_STOP_SCHEDULE_DELETE = '/api/bus-stop-schedule/delete';
  URL_BUS_STOP_SCHEDULE_GET = '/api/bus-stop-schedule/item';
  URL_BUS_STOP_SCHEDULE_LIST_GET = '/api/bus-stop-schedule/list';

  filter: BusStopScheduleFilter = {
    pageIndex: 0,
    pageSize: 20,
    sortBy: 'code',
    ascSort: true,
    keyword: '',
    tenantId: this.tokenService.getTenantId()
  }

  isLoading: boolean = false;
  isEmpty: boolean = false;
  busStopSchedules: BusStopSchedule[] = [];
  filteredBusStopSchedules: BusStopSchedule[] = [];

  search$: Subject<void> = new Subject<void>();
  cancel$: Subject<void> = new Subject<void>();

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
  ) { }

  //#region create bus stop schedule
  createBusStopSchedule(busStopSchedule: BusStopSchedule): Promise<BusStopSchedule> {
    return new Promise<BusStopSchedule>((resolve, reject) => {
      const url = new URL(this.URL_BUS_STOP_SCHEDULE_CREATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busStopSchedule.id);
      formData.append('tenantId', busStopSchedule.tenantId);
      formData.append('busStopId', busStopSchedule.busStopId);
      formData.append('routeCode', busStopSchedule.routeCode);
      formData.append('operatorCode', busStopSchedule.operatorCode);
      formData.append('bus1OriginCode', busStopSchedule.bus1OriginCode);
      formData.append('bus1DestCode', busStopSchedule.bus1DestCode);
      formData.append('bus1Eta', busStopSchedule.bus1Eta);
      formData.append('bus1Lat', busStopSchedule.bus1Lat.toString());
      formData.append('bus1Lng', busStopSchedule.bus1Lng.toString());
      formData.append('bus1Load', busStopSchedule.bus1Load);
      formData.append('bus1Wheelchair', String(busStopSchedule.bus1Wheelchair));
      formData.append('bus1Type', busStopSchedule.bus1Type);
      formData.append('bus2OriginCode', busStopSchedule.bus2OriginCode);
      formData.append('bus2DestCode', busStopSchedule.bus2DestCode);
      formData.append('bus2Eta', busStopSchedule.bus2Eta);
      formData.append('bus2Lat', busStopSchedule.bus2Lat.toString());
      formData.append('bus2Lng', busStopSchedule.bus2Lng.toString());
      formData.append('bus2Load', busStopSchedule.bus2Load);
      formData.append('bus2Wheelchair', String(busStopSchedule.bus2Wheelchair));
      formData.append('bus2Type', busStopSchedule.bus2Type);
      formData.append('bus3OriginCode', busStopSchedule.bus3OriginCode);
      formData.append('bus3DestCode', busStopSchedule.bus3DestCode);
      formData.append('bus3Eta', busStopSchedule.bus3Eta);
      formData.append('bus3Lat', busStopSchedule.bus3Lat.toString());
      formData.append('bus3Lng', busStopSchedule.bus3Lng.toString());
      formData.append('bus3Load', busStopSchedule.bus3Load);
      formData.append('bus3Wheelchair', String(busStopSchedule.bus3Wheelchair));
      formData.append('bus3Type', busStopSchedule.bus3Type);
      formData.append('lastUpdate', busStopSchedule.lastUpdate);

      this.http.post<BusStopSchedule>(url, formData)
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region update bus stop schedule
  updateBusStopSchedule(busStopSchedule: BusStopSchedule): Promise<BusStopSchedule> {
    return new Promise<BusStopSchedule>((resolve, reject) => {
      const url = new URL(this.URL_BUS_STOP_SCHEDULE_UPDATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busStopSchedule.id);
      formData.append('tenantId', busStopSchedule.tenantId);
      formData.append('busStopId', busStopSchedule.busStopId);
      formData.append('routeCode', busStopSchedule.routeCode);
      formData.append('operatorCode', busStopSchedule.operatorCode);
      formData.append('bus1OriginCode', busStopSchedule.bus1OriginCode);
      formData.append('bus1DestCode', busStopSchedule.bus1DestCode);
      formData.append('bus1Eta', busStopSchedule.bus1Eta);
      formData.append('bus1Lat', busStopSchedule.bus1Lat.toString());
      formData.append('bus1Lng', busStopSchedule.bus1Lng.toString());
      formData.append('bus1Load', busStopSchedule.bus1Load);
      formData.append('bus1Wheelchair', String(busStopSchedule.bus1Wheelchair));
      formData.append('bus1Type', busStopSchedule.bus1Type);
      formData.append('bus2OriginCode', busStopSchedule.bus2OriginCode);
      formData.append('bus2DestCode', busStopSchedule.bus2DestCode);
      formData.append('bus2Eta', busStopSchedule.bus2Eta);
      formData.append('bus2Lat', busStopSchedule.bus2Lat.toString());
      formData.append('bus2Lng', busStopSchedule.bus2Lng.toString());
      formData.append('bus2Load', busStopSchedule.bus2Load);
      formData.append('bus2Wheelchair', String(busStopSchedule.bus2Wheelchair));
      formData.append('bus2Type', busStopSchedule.bus2Type);
      formData.append('bus3OriginCode', busStopSchedule.bus3OriginCode);
      formData.append('bus3DestCode', busStopSchedule.bus3DestCode);
      formData.append('bus3Eta', busStopSchedule.bus3Eta);
      formData.append('bus3Lat', busStopSchedule.bus3Lat.toString());
      formData.append('bus3Lng', busStopSchedule.bus3Lng.toString());
      formData.append('bus3Load', busStopSchedule.bus3Load);
      formData.append('bus3Wheelchair', String(busStopSchedule.bus3Wheelchair));
      formData.append('bus3Type', busStopSchedule.bus3Type);
      formData.append('lastUpdate', busStopSchedule.lastUpdate);

      this.http.post<BusStopSchedule>(url, formData)
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus stop schedule list
  getBusStopScheduleList(): Promise<BusStopSchedule[]> {
    this.cancel$.next()

    return new Promise<BusStopSchedule[]>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_STOP_SCHEDULE_LIST_GET, env.bimsApiEndpoint).toString();

      this.http.post<BusStopSchedule[]>(url, this.filter)
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.cancel$.complete();
            this.isLoading = false;
          },
          error: (err) => {
            reject(new Error(err.message));
            this.cancel$.complete();
            this.isLoading = false;
          },
        });
    })
  }

  processBusList(res: any) {
    this.busStopSchedules = res.data;
    this.filterBusStopSchedules();
    
    if (this.busStopSchedules.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }

  OnDestroy() {
    this.cancel$.next();
    this.cancel$.complete();
    this.filter.keyword = '';
  }

  search() {
    this.search$.next();
    this.isLoading = true;
  }

  filterBusStopSchedules() {
    this.filteredBusStopSchedules = this.busStopSchedules.filter(x =>
      x.routeCode.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.operatorCode.toLowerCase().includes(this.filter.keyword.toLowerCase())
    );

    if (this.filteredBusStopSchedules.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }
  //#endregion

  //#region delete bus stop schedule
  deleteBusStopSchedule(id: string): Promise<Result> {
    return new Promise<Result>((resolve, reject) => {
      const url = new URL(this.URL_BUS_STOP_SCHEDULE_DELETE, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', id)

      this.http.delete<any>(url, { params: params })
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            res.message = 'success'
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

}
