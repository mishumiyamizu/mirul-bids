import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/app/models/env';
import { Result } from 'src/app/models/result';
import { Observable, Subject, delay, retryWhen } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from './token.service';
import { BusStop, BusStopFilter } from '../models/bus-stop';
import { FileUploadService } from './file-upload.service';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class BusStopService {
  URL_BUS_STOP_CREATE = '/api/bus-stop/create';
  URL_BUS_STOP_UPDATE = '/api/bus-stop/update';
  URL_BUS_STOP_DELETE = '/api/bus-stop/delete';
  URL_BUS_STOP_GET = '/api/bus-stop/item';
  URL_BUS_STOP_LIST_GET = '/api/bus-stop/list';

  filter: BusStopFilter = {
    pageIndex: 0,
    pageSize: 20,
    sortBy: 'code',
    ascSort: true,
    keyword: '',
    tenantId: this.tokenService.getTenantId(),
  }

  isLoading: boolean = false
  isEmpty: boolean = false;
  busStops: BusStop[] = [];
  filteredBusStops: BusStop[] = [];

  search$: Subject<void> = new Subject<void>();
  cancel$: Subject<void> = new Subject<void>();

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public fileUploadService: FileUploadService
  ) { }


  //#region create bus stop
  createBusStop(busStop: BusStop): Promise<BusStop> {
    return new Promise<BusStop>((resolve, reject) => {
      const url = new URL(this.URL_BUS_STOP_CREATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busStop.id);
      formData.append('tenantId', busStop.tenantId);
      formData.append('code', busStop.code);
      formData.append('name', busStop.name);
      formData.append('latitude', busStop.latitude.toString());
      formData.append('longitude', busStop.longitude.toString());
      formData.append('roadName', busStop.roadName);
      formData.append('landmark', busStop.landmark);
      formData.append('status', busStop.status);
      formData.append('country', busStop.country);
      formData.append('city', busStop.city);
      formData.append('timezoneUTC', busStop.timezoneUTC);
      formData.append('lastUpdate', busStop.lastUpdate);
      formData.append('updatedBy', busStop.updatedBy);

      if (this.fileUploadService.file)
        formData.append('img', this.fileUploadService.file);

      this.http.post<BusStop>(url, formData)
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region update bus stop
  updateBusStop(busStop: BusStop): Promise<BusStop> {
    return new Promise<BusStop>((resolve, reject) => {
      const url = new URL(this.URL_BUS_STOP_UPDATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      if (!this.fileUploadService.imageSrc)
        busStop.avatar = '';

      formData.append('id', busStop.id);
      formData.append('tenantId', busStop.tenantId);
      formData.append('code', busStop.code);
      formData.append('name', busStop.name);
      formData.append('latitude', busStop.latitude.toString());
      formData.append('longitude', busStop.longitude.toString());
      formData.append('roadName', busStop.roadName);
      formData.append('landmark', busStop.landmark);
      formData.append('status', busStop.status);
      formData.append('country', busStop.country);
      formData.append('city', busStop.city);
      formData.append('timezoneUTC', busStop.timezoneUTC);
      formData.append('lastUpdate', busStop.lastUpdate);
      formData.append('updatedBy', busStop.updatedBy);
      formData.append('avatar', busStop.avatar);

      if (this.fileUploadService.file)
        formData.append('img', this.fileUploadService.file);

      this.http.post<BusStop>(url, formData)
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus stop list
  getBusStopList(): Promise<BusStop[]> {
    this.cancel$.next();

    return new Promise<BusStop[]>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_STOP_LIST_GET, env.bimsApiEndpoint).toString();

      this.http.post<BusStop[]>(url, this.filter)
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.cancel$.complete();
            this.isLoading = false;
          },
          error: (err) => {
            reject(new Error(err.message));
            this.cancel$.complete();
            this.isLoading = false;
          },
        });
    })
  }

  processBusStopList(res: any) {
    this.busStops = res.data;
    this.filterBusStops();
    this.fileUploadService.processImages(res.data); //important to get img, otherwise, it wont get img

    if (this.busStops.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }

  OnDestroy() {
    this.cancel$.next();
    this.cancel$.complete();
    this.filter.keyword = '';
  }

  search() {
    this.search$.next();
    this.isLoading = true;
  }

  filterBusStops() {
    this.filteredBusStops = this.busStops.filter(x =>
      x.code.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.name.toLowerCase().includes(this.filter.keyword.toLowerCase())
    );

    if (this.filteredBusStops.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }
  //#endregion

  //#region get bus stop item
  getBusStop(id: string): Promise<BusStop> {
    return new Promise<BusStop>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_STOP_GET, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', id);

      this.http.get<BusStop>(url, { params: params })
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            resolve(res);
            this.isLoading = false;
          },
          error: (err) => {
            reject(new Error(err.message));
            this.isLoading = false;
          },
        });
    })
  }
  //#endregion

  //#region delete bus stop
  deleteBusStop(busStopId: string): Promise<Result> {
    return new Promise<Result>((resolve, reject) => {
      const url = new URL(this.URL_BUS_STOP_DELETE, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', busStopId)

      this.http.delete<any>(url, { params: params })
        .pipe(retryWhen((err: Observable<any>) => err.pipe(delay(10000))))
        .subscribe({
          next: (res) => {
            res.message = 'success'
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion
}
