import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { HttpClient } from '@angular/common/http';
import { map, Subject } from 'rxjs';
import * as moment from 'moment';
import { LoggerService } from './logger.service';
import { Env } from 'src/app/models/env';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  URL_RENEW_ACCESS_TOKEN = '/api/account/renew-access-token';
  URL_RENEW_REFRESH_TOKEN = '/api/account/renew-refresh-token';

  accessToken: { [key: string]: string };
  refreshToken: { [key: string]: string };

  accessTokenSubject = new Subject<string>();
  refreshTokenSubject = new Subject<string>();

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private logger: LoggerService,
    private http: HttpClient
  ) {
    this.accessToken = {};
    this.refreshToken = {};
  }

  get accessTokenStr() {
    var str = localStorage.getItem('access-token');
    if (str)
      return str;
    else
      return '';
  }

  get refreshTokenStr() {
    var str = localStorage.getItem('refresh-token');
    if (str)
      return str;
    else
      return '';
  }

  onAccessTokenUpdated(){
    return this.accessTokenSubject;
  }

  onRefreshTokenUpdated(){
    return this.refreshTokenSubject;
  }

  loadAccessToken() {
    let token = localStorage.getItem('access-token');
    console.log(`load access token from local storage: ${token}`);

    if (token) {
      this.accessToken = jwt_decode(token);

      console.log(this.accessToken);
      console.log(`access token expiry time: ${this.getAccessTokenExpiryTime()}`);
    }
  }

  loadRefreshToken() {
    let token = localStorage.getItem('refresh-token');
    console.log(`load refresh token from local storage: ${token}`);

    if (token) {
      this.refreshToken = jwt_decode(token);

      console.log(this.refreshToken);
      console.log(`refresh token expiry time: ${this.getRefreshTokenExpiryTime()}`);
    }
  }

  setAccessToken(token: string) {
    console.log(`set access token： ${token}`);

    if (token) {    
      this.accessToken = jwt_decode(token);    
      localStorage.setItem('access-token', token);
      this.accessTokenSubject.next('renewed');

      console.log(this.accessToken);
      console.log(`access token expiry time: ${this.getAccessTokenExpiryTime()}`);
    }
  }

  clearAccessToken() {
    this.accessToken = {};
    localStorage.removeItem('access-token');
    this.accessTokenSubject.next('cleared');
  }

  setRefreshToken(token: string) {
    console.log(`set refresh token： ${token}`);

    if (token) {
      this.refreshToken = jwt_decode(token);    
      localStorage.setItem('refresh-token', token);
      this.refreshTokenSubject.next('renewed');

      console.log(this.refreshToken);
      console.log(`refresh token expiry time: ${this.getRefreshTokenExpiryTime()}`);
    }
  }

  clearRefreshToken() {
    this.refreshToken = {};
    localStorage.removeItem('refresh-token');
    this.refreshTokenSubject.next('cleared');
  }

  getEmail() {
    const email = this.accessToken['email'];
    if (email)
      return email;
    else
      return '';
  }

  getThemeColor() {
    const themeColor = this.accessToken['theme-color'];
    if (themeColor) {
      return themeColor;
    } else {
      return 'theme-blue';
    }
  }

  getThemeMode() {
    const themeMode = this.accessToken['theme-mode'];
    if (themeMode)
      return themeMode;
    else
      return 'light-mode';
  }

  getLanguage() {
    const lang = this.accessToken['language'];
    if (lang)
      return lang;
    else
      return 'en';
  }

  getName() {
    const name = this.accessToken['name'];
    if (name)
      return name;
    else
      return '';
  }

  getPhoneNumber() {
    const phoneNumber = this.accessToken['phone-number'];
    if (phoneNumber)
      return phoneNumber;
    else
      return '';
  }

  getComponent() {
    const val = this.accessToken['component'];
    if (val)
      return val;
    else
      return '';
  }

  getTenantId() {
    const tenantId = this.accessToken['tenant-id'];
    if (tenantId)
      return tenantId;
    else
      return '';
  }

  getTimeZone() {
    const val = this.accessToken['time-zone'];
    if (val) {
      return val;
    } else {
      let timeZone = Intl.DateTimeFormat(this.locale).resolvedOptions().timeZone;
      return timeZone;
    }
  }

  getAccessTokenExpiryTime(raw: boolean = false) {
    if (raw) {
      return this.accessToken ? this.accessToken['exp'] : null; //the raw time is number of seconds sinc jan 1 1970
    } else {
      return this.accessToken ? moment(parseInt(this.accessToken['exp']) * 1000).format('DD/MM/YYYY HH:mm:ss') : null;
    }
  }

  getRefreshTokenExpiryTime(raw: boolean = false) {
    if (raw) {
      return this.refreshToken ? this.refreshToken['exp'] : null; //the raw time is number of seconds sinc jan 1 1970
    } else {
      return this.refreshToken ? moment(parseInt(this.refreshToken['exp']) * 1000).format('DD/MM/YYYY HH:mm:ss') : null;
    }
  }

  checkIsAccessTokenExpired() {
    const expiryTime = moment(parseInt(this.accessToken['exp']) * 1000);
    const currentTime = moment();
    if (expiryTime.diff(currentTime) > 0) {
      return false;
    } else {
      return true;
    }
  }

  checkIsRefreshTokenExpired() {
    const expiryTime = moment(parseInt(this.refreshToken['exp']) * 1000);
    const currentTime = moment();
    if (expiryTime.diff(currentTime) > 0) {
      return false;
    } else {
      return true;
    }
  }

  checkIsAuthenticated(): boolean {
    if (this.accessToken && Object.keys(this.accessToken).length != 0) {
      return true; //return true as long as has token. the actual authentication will be done when got api call. for ui just return true if has access token
    } else {
      return false;
    }
  }

  hasClaim(claim: string): boolean {
    if (this.accessToken && this.accessToken['permission']) {
      if (this.accessToken['permission'] === '*') {
        return true;
      } else {
        let hasPermission = this.accessToken['permission'].toLowerCase().includes(claim.toLocaleLowerCase());
        return hasPermission;
      }
    } else {
      return false;
    }
  }

  renewAccessToken() {
    this.logger.debug('renew access token');

    const url = new URL(this.URL_RENEW_ACCESS_TOKEN, env.identityApiEndpoint).toString();
    const formData = new FormData();
    formData.append('refreshToken', this.refreshTokenStr);

    return this.http.post<string>(url, formData).pipe(map((token)=> this.setAccessToken(token)));
  }

  renewRefreshToken() {
    this.logger.debug('renew refresh token');

    const url = new URL(this.URL_RENEW_REFRESH_TOKEN, env.identityApiEndpoint).toString();
    const formData = new FormData();
    formData.append('refreshToken', this.refreshTokenStr);

    return this.http.post(url, formData);
  }


}
