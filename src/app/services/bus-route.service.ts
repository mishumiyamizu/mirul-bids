import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/app/models/env';
import { Result } from 'src/app/models/result';
import { Observable, Subject, delay, retryWhen } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from './token.service';
import { BusRoute, BusRouteFilter } from '../models/bus-route';
import { BusStop } from '../models/bus-stop';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class BusRouteService {
  URL_BUS_ROUTE_CREATE = '/api/bus-route/create';
  URL_BUS_ROUTE_UPDATE = '/api/bus-route/update';
  URL_BUS_ROUTE_DELETE = '/api/bus-route/delete';
  URL_BUS_ROUTE_GET = '/api/bus-route/item';
  URL_BUS_ROUTE_LIST_GET = '/api/bus-route/list';
  URL_BUS_ROUTE_LIST_BUS_STOP = '/api/bus-route/list-with-bus-stop';

  filter: BusRouteFilter = {
    pageIndex: 0,
    pageSize: 20,
    sortBy: 'code',
    ascSort: true,
    keyword: '',
    tenantId: this.tokenService.getTenantId(),
    code: '',
  }

  isLoading: boolean = false;
  isEmpty: boolean = false;
  busRoutes: BusRoute[] = [];
  // busRoutesWithBusStop: BusRoute[] = [];
  filteredBusRoutes: BusRoute[] = [];

  search$: Subject<void> = new Subject<void>();
  cancel$: Subject<void> = new Subject<void>();

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService
  ) { }

  //#region create bus route
  createBusRoute(busRoute: BusRoute): Promise<BusRoute> {
    return new Promise<BusRoute>((resolve, reject) => {
      const url = new URL(this.URL_BUS_ROUTE_CREATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busRoute.id);
      formData.append('tenantId', busRoute.tenantId);
      formData.append('code', busRoute.code);
      formData.append('name', busRoute.name);
      formData.append('operatorCode', busRoute.operatorCode);
      formData.append('totalbusStop', busRoute.totalBusStop.toString());
      formData.append('originBusStop', JSON.stringify(busRoute.originBusStop));
      formData.append('destBusStop', JSON.stringify(busRoute.destBusStop));
      formData.append('directionToOrigin', JSON.stringify(busRoute.directionToOrigin));
      formData.append('directionToDest', JSON.stringify(busRoute.directionToDest));
      formData.append('lastUpdate', busRoute.lastUpdate);
      formData.append('updatedBy', busRoute.updatedBy);

      this.http.post<BusRoute>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region update bus route
  updateBusRoute(busRoute: BusRoute): Promise<BusRoute> {
    return new Promise<BusRoute>((resolve, reject) => {
      const url = new URL(this.URL_BUS_ROUTE_UPDATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busRoute.id);
      formData.append('tenantId', busRoute.tenantId);
      formData.append('code', busRoute.code);
      formData.append('name', busRoute.name);
      formData.append('operatorCode', busRoute.operatorCode);
      formData.append('totalbusStop', busRoute.totalBusStop.toString());
      formData.append('originBusStop', JSON.stringify(busRoute.originBusStop));
      formData.append('destBusStop', JSON.stringify(busRoute.destBusStop));
      formData.append('directionToOrigin', JSON.stringify(busRoute.directionToOrigin));
      formData.append('directionToDest', JSON.stringify(busRoute.directionToDest));
      formData.append('lastUpdate', busRoute.lastUpdate);
      formData.append('updatedBy', busRoute.updatedBy);

      this.http.post<BusRoute>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus route list
  getBusRouteList(): Promise<BusRoute[]> {
    this.cancel$.next();

    return new Promise<BusRoute[]>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_ROUTE_LIST_GET, env.bimsApiEndpoint).toString();

      this.http.post<BusRoute[]>(url, this.filter)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.cancel$.complete();
            this.isLoading = false;
          },
          error: (err) => {
            reject(new Error(err.message));
            this.cancel$.complete();
            this.isLoading = false;
          },
        });
    })
  }

  processBusRouteList(res: any) {
    res.data.forEach(((x: { originBusStop: string; destBusStop: string; directionToOrigin: string; directionToDest: string; }) => {
      x.originBusStop = JSON.parse(x.originBusStop);
      x.destBusStop = JSON.parse(x.destBusStop);
      x.directionToOrigin = JSON.parse(x.directionToOrigin);
      x.directionToDest = JSON.parse(x.directionToDest);
    }));

    this.busRoutes = res.data;
    console.log(this.busRoutes)
    
    this.filterBusRoutes();

    if (this.busRoutes.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }

  OnDestroy() {
    this.cancel$.next();
    this.cancel$.complete();
    this.filter.keyword = '';
  }

  search() {
    this.search$.next();
    this.isLoading = true;
  }

  filterBusRoutes() {
    this.filteredBusRoutes = this.busRoutes.filter(x =>
      x.code.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.operatorCode.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.name.toLowerCase().includes(this.filter.keyword.toLowerCase())
    );

    if (this.filteredBusRoutes.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }
  //#endregion

  //#region delete bus route
  deleteBusRoute(busRouteId: string): Promise<Result> {
    return new Promise<Result>((resolve, reject) => {
      const url = new URL(this.URL_BUS_ROUTE_DELETE, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', busRouteId)

      this.http.delete<any>(url, { params: params })
        .subscribe({
          next: (res) => {
            res.message = 'success'
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus route list
  // getBusRouteListWithBusStop(code: string): Promise<BusRoute[]> {
  //   return new Promise<BusRoute[]>((resolve, reject) => {
  //     this.filter.code = code;
  //     const url = new URL(this.URL_BUS_ROUTE_LIST_BUS_STOP, this.env.bimsApiEndpoint).toString();

  //     this.http.post<BusRoute[]>(url, this.filter)
  //       
  //       .subscribe({
  //         next: (res) => {
  //           resolve(res);
  //         },
  //         error: (err) => {
  //           reject(new Error(err.message));
  //         },
  //       });
  //   })
  // }

  // processBusRouteListWithBusStop(res: any) {
  //   this.busRoutesWithBusStop = res.data;
  //   console.log(this.busRoutesWithBusStop)

  //   this.destRoute = this.busRoutesWithBusStop.filter((busRoute: { direction: number; }) => busRoute.direction === 3)
  //   this.destBusStop = this.destRoute[0].busStop
  //   console.log(this.destBusStop)

  //   this.originRoute = this.busRoutesWithBusStop.filter((busRoute: { busStopSequence: number; }) => busRoute.busStopSequence === 0)
  //   this.originBusStop = this.originRoute[0].busStop
  //   console.log(this.originBusStop)

  //   if (this.busRoutes.length > 0)
  //     this.isEmpty = false;
  //   else
  //     this.isEmpty = true;
  // }
}
