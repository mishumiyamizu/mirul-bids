import { Injectable } from '@angular/core';
import { BusTerminalSchedule, BusTerminalScheduleFilter } from '../models/bus-terminal-schedule';
import { Subject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Env } from 'src/app/models/env';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from './token.service';
import { FileUploadService } from './file-upload.service';
import { Result } from '../models/result';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class BusTerminalScheduleService {

  URL_BUS_TERMINAL_CREATE = '/api/terminal-bus-schedule/create';
  URL_BUS_TERMINAL_UPDATE = '/api/terminal-bus-schedule/update';
  URL_BUS_TERMINAL_DELETE = '/api/terminal-bus-schedule/delete';
  URL_BUS_TERMINAL_GET = '/api/terminal-bus-schedule/item';
  URL_BUS_TERMINAL_LIST_GET = '/api/terminal-bus-schedule/list';

  filter: BusTerminalScheduleFilter = {
    pageIndex: 0,
    pageSize: 20,
    sortBy: 'code',
    ascSort: true,
    keyword: '',
    tenantId: this.tokenService.getTenantId(),
    origin: '',
    destination: '',
    departureTime: ''
  }

  isLoading: boolean = false;
  isEmpty: boolean = false;
  busTerminals: BusTerminalSchedule[] = [];
  filteredBusTerminals: BusTerminalSchedule[] = [];

  search$: Subject<void> = new Subject<void>();
  cancel$: Subject<void> = new Subject<void>();

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar,
    public translate: TranslateService,
    public tokenService: TokenService,
    public fileUploadService: FileUploadService,
  ) { }

  //#region create bus
  createBusTerminalSchedule(busTerminal: BusTerminalSchedule): Promise<BusTerminalSchedule> {
    return new Promise<BusTerminalSchedule>((resolve, reject) => {
      const url = new URL(this.URL_BUS_TERMINAL_CREATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busTerminal.id);
      formData.append('tenantId', busTerminal.tenantId);
      formData.append('operatorCode', busTerminal.operatorCode);
      formData.append('tripCode', busTerminal.tripCode);
      formData.append('gate', busTerminal.gate);
      formData.append('bay', busTerminal.bay);
      formData.append('plateNo', busTerminal.plateNo);
      formData.append('departureTime', busTerminal.departureTime);
      formData.append('destination', busTerminal.destination);
      formData.append('status', busTerminal.status);
      formData.append('origin', busTerminal.origin);
      formData.append('arrivalTime', busTerminal.arrivalTime);
      formData.append('type', busTerminal.type);
      formData.append('totalSeat', busTerminal.totalSeat.toString());
      formData.append('date', busTerminal.date.toString());

      this.http.post<BusTerminalSchedule>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region update bus
  updateBusTerminalSchedule(busTerminal: BusTerminalSchedule): Promise<BusTerminalSchedule> {
    return new Promise<BusTerminalSchedule>((resolve, reject) => {
      const url = new URL(this.URL_BUS_TERMINAL_UPDATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', busTerminal.id);
      formData.append('tenantId', busTerminal.tenantId);
      formData.append('operatorCode', busTerminal.operatorCode);
      formData.append('tripCode', busTerminal.tripCode);
      formData.append('gate', busTerminal.gate);
      formData.append('bay', busTerminal.bay);
      formData.append('plateNo', busTerminal.plateNo);
      formData.append('departureTime', busTerminal.departureTime);
      formData.append('destination', busTerminal.destination);
      formData.append('status', busTerminal.status);
      formData.append('origin', busTerminal.origin);
      formData.append('arrivalTime', busTerminal.arrivalTime);
      formData.append('type', busTerminal.type);
      formData.append('totalSeat', busTerminal.totalSeat.toString());
      formData.append('date', busTerminal.date.toString());

      this.http.post<BusTerminalSchedule>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus list
  getBusTerminalScheduleList(): Promise<BusTerminalSchedule[]> {
    this.cancel$.next();

    return new Promise<BusTerminalSchedule[]>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_TERMINAL_LIST_GET, env.bimsApiEndpoint).toString();

      this.http.post<BusTerminalSchedule[]>(url, this.filter)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.cancel$.complete();
            this.isLoading = false;

          },
          error: (err) => {
            reject(new Error(err.message));
            this.cancel$.complete();
            this.isLoading = false;
          },
        });
    })
  }

  processBusTerminalScheduleList(res: any) {
    this.busTerminals = res.data;
    this.filterBusTerminalSchedules();
    //this.fileUploadService.processImages(res.data); //important to get img, otherwise, it wont get img

    if (this.busTerminals.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }

  OnDestroy() {
    this.cancel$.next();
    this.cancel$.complete();
    this.filter.keyword = '';
    this.filter.origin = '';
    this.filter.destination = '';
    this.filter.departureTime = '';
  }

  search() {
    this.search$.next();
    this.isLoading = true;
  }

  filterBusTerminalSchedules() {
    this.filteredBusTerminals = this.busTerminals.filter(x =>
      x.departureTime.toLowerCase().includes(this.filter.departureTime.toLowerCase()) &&
      x.origin.toLowerCase().includes(this.filter.origin.toLowerCase()) &&
      x.destination.toLowerCase().includes(this.filter.destination.toLowerCase())
    );

    if (this.filteredBusTerminals.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }
  //#endregion

  //#region delete bus
  deleteBusTerminalSchedule(busTerminalId: string): Promise<Result> {
    return new Promise<Result>((resolve, reject) => {
      const url = new URL(this.URL_BUS_TERMINAL_DELETE, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', busTerminalId)

      this.http.delete<any>(url, { params: params })
        .subscribe({
          next: (res) => {
            res.message = 'success'
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion
}

