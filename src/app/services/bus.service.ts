import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/app/models/env';
import { Result } from 'src/app/models/result';
import { Observable, Subject, delay, retryWhen } from 'rxjs';
import { Bus, BusFilter } from '../models/bus';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from './token.service';
import { FileUploadService } from './file-upload.service';

declare const env: Env;

@Injectable({
  providedIn: 'root',
})

export class BusService {
  URL_BUS_CREATE = '/api/bus/create';
  URL_BUS_UPDATE = '/api/bus/update';
  URL_BUS_DELETE = '/api/bus/delete';
  URL_BUS_GET = '/api/bus/item';
  URL_BUS_LIST_GET = '/api/bus/list';

  filter: BusFilter = {
    pageIndex: 0,
    pageSize: 20,
    sortBy: 'code',
    ascSort: true,
    keyword: '',
    tenantId: this.tokenService.getTenantId()
  }

  isLoading: boolean = false;
  isEmpty: boolean = false;
  buses: Bus[] = [];
  filteredBuses: Bus[] = [];

  search$: Subject<void> = new Subject<void>();
  cancel$: Subject<void> = new Subject<void>();

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar,
    public translate: TranslateService, 
    public tokenService: TokenService,
    public fileUploadService: FileUploadService,
  ) { }

  //#region create bus
  createBus(bus: Bus): Promise<Bus> {
    return new Promise<Bus>((resolve, reject) => {
      const url = new URL(this.URL_BUS_CREATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      formData.append('id', bus.id);
      formData.append('tenantId', bus.tenantId);
      formData.append('code', bus.code);
      formData.append('licensePlate', bus.licensePlate);
      formData.append('operatorCode', bus.operatorCode);
      formData.append('type', bus.type);
      formData.append('wheelchair', String(bus.wheelchair));
      formData.append('direction', bus.direction.toString());
      formData.append('routeCode', bus.routeCode);
      formData.append('originStopCode', bus.originStopCode);
      formData.append('destStopCode', bus.destStopCode);
      formData.append('amPeakFreq', bus.amPeakFreq);
      formData.append('amOffpeakFreq', bus.amOffpeakFreq);
      formData.append('pmPeakFreq', bus.pmPeakFreq);
      formData.append('pmOffpeakFreq', bus.pmOffpeakFreq);
      formData.append('lastUpdate', bus.lastUpdate);
      formData.append('updatedBy', bus.updatedBy);

      if (this.fileUploadService.file)
        formData.append('img', this.fileUploadService.file);

      this.http.post<Bus>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region update bus
  updateBus(bus: Bus): Promise<Bus> {
    return new Promise<Bus>((resolve, reject) => {
      const url = new URL(this.URL_BUS_UPDATE, env.bimsApiEndpoint).toString();
      const formData = new FormData();

      if (!this.fileUploadService.imageSrc)
        bus.avatar = '';

      formData.append('id', bus.id);
      formData.append('tenantId', bus.tenantId);
      formData.append('code', bus.code);
      formData.append('licensePlate', bus.licensePlate);
      formData.append('operatorCode', bus.operatorCode);
      formData.append('type', bus.type);
      formData.append('wheelchair', String(bus.wheelchair));
      formData.append('direction', bus.direction.toString());
      formData.append('routeCode', bus.routeCode);
      formData.append('originStopCode', bus.originStopCode);
      formData.append('destStopCode', bus.destStopCode);
      formData.append('amPeakFreq', bus.amPeakFreq);
      formData.append('amOffpeakFreq', bus.amOffpeakFreq);
      formData.append('pmPeakFreq', bus.pmPeakFreq);
      formData.append('pmOffpeakFreq', bus.pmOffpeakFreq);
      formData.append('lastUpdate', bus.lastUpdate);
      formData.append('updatedBy', bus.updatedBy);
      formData.append('avatar', bus.avatar);

      if (this.fileUploadService.file)
        formData.append('img', this.fileUploadService.file);

      this.http.post<Bus>(url, formData)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.SAVE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion

  //#region get bus list
  getBusList(): Promise<Bus[]> {
    this.cancel$.next();

    return new Promise<Bus[]>((resolve, reject) => {
      this.isLoading = true;
      const url = new URL(this.URL_BUS_LIST_GET, env.bimsApiEndpoint).toString();

    // refresh tenant ID
      this.filter.tenantId = this.tokenService.getTenantId()

      this.http.post<Bus[]>(url, this.filter)
        .subscribe({
          next: (res) => {
            resolve(res);
            this.cancel$.complete();
            this.isLoading = false;

          },
          error: (err) => {
            reject(new Error(err.message));
            this.cancel$.complete();
            this.isLoading = false;
          },
        });
    })
  }

  processBusList(res: any) {
    this.buses = res.data;
    this.filterBuses();
    this.fileUploadService.processImages(res.data); //important to get img, otherwise, it wont get img

    if (this.buses.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }

  OnDestroy() {
    this.cancel$.next();
    this.cancel$.complete();
    this.filter.keyword = '';
  }

  search() {
    this.search$.next();
    this.isLoading = true;
  }

  filterBuses() {
    this.filteredBuses = this.buses.filter(x =>
      x.code.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.operatorCode.toLowerCase().includes(this.filter.keyword.toLowerCase()) ||
      x.licensePlate.toLowerCase().includes(this.filter.keyword.toLowerCase())
    );

    if (this.filteredBuses.length > 0)
      this.isEmpty = false;
    else
      this.isEmpty = true;
  }
  //#endregion

  //#region delete bus
  deleteBus(busId: string): Promise<Result> {
    return new Promise<Result>((resolve, reject) => {
      const url = new URL(this.URL_BUS_DELETE, env.bimsApiEndpoint).toString();
      const params = new HttpParams()
        .append('id', busId)

      this.http.delete<any>(url, { params: params })
        .subscribe({
          next: (res) => {
            res.message = 'success'
            resolve(res);
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_SUCCESS'), '', {
              duration: 3000
            });
          },
          error: (err) => {
            reject(new Error(err.message));
            this.snackBar.open(this.translate.instant('GENERAL.DELETE_ERROR'), '', {
              duration: 3000
            });
          },
        });
    })
  }
  //#endregion
}
