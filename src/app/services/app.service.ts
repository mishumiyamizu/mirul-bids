import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { retry, Subject, switchMap } from 'rxjs';
import { TokenService } from './token.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { LoggerService } from './logger.service';
import { ServiceComponent } from '../models/service-component';
import { Env } from 'src/app/models/env';

declare const env: Env;

@Injectable({
  providedIn: 'root'
})
export class AppService {

  avatar: string = '';

  isSidebarOn: boolean = true;

  URL_UPDATE_THEME_COLOR = '/api/account/update-theme-color';
  URL_UPDATE_THEME_MODE = '/api/account/update-theme-mode';
  URL_UPDATE_LANGUAGE = '/api/account/update-language';
  URL_GET_AVATAR = '/api/account/get-avatar';
  URL_SIGN_OUT = '/api/account/sign-out';
  URL_REPORT = '/api/report/item';
  URL_SERVICE_COMPONENT_LIST = '/api/service-component/list';

  DEFAULT_THEME = 'theme-blue';

  currentThemeColor = this.DEFAULT_THEME;
  currentThemeMode = 'light-mode';
  currentLanguage = 'en';

  isLoginPageResetDone = false;

  @Output() onLanguageChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() onThemeColorChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() onThemeModeChanged: EventEmitter<string> = new EventEmitter<string>();

  private themeMode$ = new Subject<string>();
  private themeColor$ = new Subject<string>();
  private language$ = new Subject<string>();

  serviceComponentArray: ServiceComponent[] = [];
  serviceComponentSubscribedArray: ServiceComponent[] = [];

  constructor(
    public http: HttpClient,
    public router: Router,
    public tokenService: TokenService,
    public translate: TranslateService,
    public loggerService: LoggerService,
    public overlayContainer: OverlayContainer) {

    this.listenMessage();
    this.initThemeColor();
    this.initThemeMode();
    this.initLanguage();

  }

  listenMessage() {
    window.addEventListener("message", (e) => {
      this.loggerService.debug(e);
      if (e.data.msgCode) {
        switch (e.data.msgCode) {
          case 'theme-color':
            this.currentThemeColor = e.data.themeColor;
            if (this.currentThemeColor !== this.DEFAULT_THEME) {
              this.lazyLoadTheme(this.currentThemeColor);
            }
            this.updateBodyTheme();
            this.updateOverlayTheme();
            this.onThemeColorChanged.emit(this.currentThemeColor);
            break;
          case 'theme-mode':
            this.currentThemeMode = e.data.themeMode;
            this.updateBodyTheme();
            this.updateOverlayTheme();
            this.onThemeModeChanged.emit(this.currentThemeMode);
            break;
          case 'language':
            this.currentLanguage = e.data.language;
            this.translate.use(this.currentLanguage).subscribe(() => {
              this.onLanguageChanged.emit(this.currentLanguage);
            });
            break;
        }
      }
    })
  }

  updateOverlayTheme() {
    const overlayContainerElement = this.overlayContainer.getContainerElement();
    overlayContainerElement.className = 'cdk-overlay-container';
    overlayContainerElement.classList.add(this.currentThemeColor, this.currentThemeMode);
  }

  updateBodyTheme() {
    const body = document.querySelector('body');
    if (body) {
      body.className = 'mat-typography';
      body.classList.add(this.currentThemeColor, this.currentThemeMode);
    }
  }

  signOut() {
    this.isLoginPageResetDone = false;
    const url = new URL(this.URL_SIGN_OUT, env.identityApiEndpoint).toString();
    this.http.get(url).subscribe(() => { });
    this.tokenService.clearAccessToken();
    this.tokenService.clearRefreshToken();
    this.router.navigateByUrl("login");
  }

  resetDefaultThemeAndLang() {
    this.resetLanguage();
    this.resetThemeColor();
    this.resetThemeMode();
  }

  //#region theme color

  initThemeColor() {
    this.loadThemeColorFromToken();
    this.themeColor$.pipe(
      switchMap((themeColor: string) => { return this.saveThemeColor(themeColor); }),
      retry()
    ).subscribe(() => {
      this.tokenService.renewAccessToken().subscribe();
    });
  }

  loadThemeColorFromToken() {
    this.currentThemeColor = this.tokenService.getThemeColor();
    if (this.currentThemeColor !== this.DEFAULT_THEME) {
      this.lazyLoadTheme(this.currentThemeColor);
    }
    this.updateBodyTheme();
    this.updateOverlayTheme();
  }

  resetThemeColor() {
    this.currentThemeColor = this.DEFAULT_THEME;
    this.updateBodyTheme();
    this.updateOverlayTheme();
  }

  updateThemeColor(themeColor: string) {
    this.currentThemeColor = themeColor;
    if (this.currentThemeColor !== this.DEFAULT_THEME) {
      this.lazyLoadTheme(this.currentThemeColor);
    }
    this.themeColor$.next(this.currentThemeColor);
    this.onThemeColorChanged.emit(this.currentThemeColor);
    this.updateBodyTheme();
    this.updateOverlayTheme();
  }

  saveThemeColor(themeColor: string) {
    const url = new URL(this.URL_UPDATE_THEME_COLOR, env.identityApiEndpoint).toString();
    const formData = new FormData();
    formData.append('themeColor', themeColor);
    return this.http.post(url, formData);
  }

  lazyLoadTheme(theme: string) {
    const id = 'lazy_load_theme';
    const link = document.getElementById(id);

    if (!link) {
      const linkEl = document.createElement('link');
      linkEl.setAttribute('rel', 'stylesheet');
      linkEl.setAttribute('type', 'text/css');
      linkEl.setAttribute('id', id);
      linkEl.setAttribute('href', `/${theme}.css`);
      document.head.appendChild(linkEl);
    } else {
      (link as HTMLLinkElement).href = `${theme}.css`;
    }
  }

  //#endregion

  //#region theme mode

  initThemeMode() {
    this.loadThemeModeFromToken();
    this.themeMode$.pipe(
      switchMap((themeMode: string) => { return this.saveThemeMode(themeMode); }),
      retry()
    ).subscribe(() => {
      this.tokenService.renewAccessToken().subscribe();
    });
  }

  loadThemeModeFromToken() {
    this.currentThemeMode = this.tokenService.getThemeMode();
    this.updateBodyTheme();
    this.updateOverlayTheme();
  }

  resetThemeMode() {
    this.currentThemeMode = 'light-mode';
    this.updateBodyTheme();
    this.updateOverlayTheme();
  }

  updateThemeMode(themeMode: string) {
    this.currentThemeMode = themeMode;
    this.themeMode$.next(this.currentThemeMode);
    this.onThemeModeChanged.emit(this.currentThemeMode);
    this.updateBodyTheme();
    this.updateOverlayTheme();
  }

  saveThemeMode(themeMode: string) {
    const url = new URL(this.URL_UPDATE_THEME_MODE, env.identityApiEndpoint).toString();
    const formData = new FormData();
    formData.append('themeMode', themeMode);
    return this.http.post(url, formData);
  }

  //#endregion

  //#region language

  initLanguage() {
    this.loadLanguageFromToken();
    this.language$.pipe(
      switchMap((language: string) => { return this.saveLanguage(language); }),
      retry()
    ).subscribe(() => {
      this.tokenService.renewAccessToken().subscribe();
    });
  }

  loadLanguageFromToken() {
    this.currentLanguage = this.tokenService.getLanguage();
    this.translate.use(this.currentLanguage).subscribe();
  }

  resetLanguage() {
    this.currentLanguage = 'en';
    this.translate.use(this.currentLanguage).subscribe();
  }

  updateLanguage(language: string) {
    this.currentLanguage = language;
    this.translate.use(this.currentLanguage).subscribe();
    this.language$.next(this.currentLanguage);
    this.onLanguageChanged.emit(this.currentLanguage);
  }

  saveLanguage(language: string) {
    const url = new URL(this.URL_UPDATE_LANGUAGE, env.identityApiEndpoint).toString();
    const formData = new FormData();
    formData.append('language', language);
    return this.http.post(url, formData);
  }

  //#endregion

  //#region avatar

  getAvatar() {
    const url = env.identityApiEndpoint + this.URL_GET_AVATAR;
    this.http.get<string>(url).subscribe((fileUrl) => {
      if (fileUrl) {
        this.avatar = env.identityApiEndpoint + '/' + fileUrl;
      } else {
        this.avatar = '';
      }
    });
  }

  //#endregion

  //#region report 

  downloadReport(apiEndpoint: string, fileName: string) {
    const url = apiEndpoint + this.URL_REPORT;
    const params = new HttpParams()
      .append('fileName', fileName);
    const headers = new HttpHeaders({
      'Accept': 'application/octet-stream'
    });

    this.http.get(url, { 'params': params, 'headers': headers, 'responseType': 'blob' }).subscribe((response: any) => {
      const a = document.createElement('a');
      document.body.appendChild(a);
      const blob: any = new Blob([response], { type: 'octet/stream' });
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    });
  }

  //#endregion

  //#region service subscribed

  loadServiceComponentList() {
    const url = new URL(this.URL_SERVICE_COMPONENT_LIST, env.identityApiEndpoint).toString();
    this.http.get<ServiceComponent[]>(url).pipe(
    ).subscribe((res) => {
      this.serviceComponentArray = res; 
     this.serviceComponentArray.forEach((item)=>{
      if(item.img){
        item.img = 'assets/img/system/' + item.img;
      }
     })
      this.updateServiceComponentSubscribed();
    })
  }

  updateServiceComponentSubscribed() {
    let subscribedComponent = this.tokenService.getComponent();
    for (let i = 0; i < this.serviceComponentArray.length; i++) {
      if (subscribedComponent === '*') {
        this.serviceComponentArray[i].subscribed = true;
      } else if (subscribedComponent.includes(this.serviceComponentArray[i].id)) {
        this.serviceComponentArray[i].subscribed = true;
      } else {
        this.serviceComponentArray[i].subscribed = false;
      }
    }

    this.serviceComponentSubscribedArray = this.serviceComponentArray.filter(x => x.subscribed);
  }

  //#endregion

}
