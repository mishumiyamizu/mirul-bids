export class ServiceComponent {
    id: string = '';
    name: string = '';
    img: string = '';
    enable: boolean = true;
    sequence: number = 0;
    subscribed: boolean = false; // for actual subscription state that is from token.
    selected:boolean = false; // for data binding in ui selection
    url: string = '';
    newTab: boolean = true;
    thirdParty: boolean = false;
}