import { BusStop } from "./bus-stop";

export class BusStopSchedule {
  id: string = '';
  tenantId: string = '';
  busStopId: string = '';
  routeCode: string = '';
  operatorCode: string = '';
  bus1OriginCode: string = '';
  bus1DestCode: string = ''
  bus1Eta: string = '';
  bus1Lat: number = 0;
  bus1Lng: number = 0;
  bus1Load: string = '';
  bus1Wheelchair: boolean = false;
  bus1Type: string = '';
  bus2OriginCode: string = '';
  bus2DestCode: string = ''
  bus2Eta: string = '';
  bus2Lat: number = 0;
  bus2Lng: number = 0;
  bus2Load: string = '';
  bus2Wheelchair: boolean = false;
  bus2Type: string = '';
  bus3OriginCode: string = '';
  bus3DestCode: string = ''
  bus3Eta: string = '';
  bus3Lat: number = 0;
  bus3Lng: number = 0;
  bus3Load: string = '';
  bus3Wheelchair: boolean = false;
  bus3Type: string = '';
  lastUpdate: string = '';
  busStop: BusStop = new BusStop();
}

export class BusStopScheduleFilter {
  tenantId: string = '';
  pageIndex: number = 0;
  pageSize: number = 20;
  sortBy: string = 'busStopCode';
  ascSort: boolean = true;
  keyword: string = '';
}