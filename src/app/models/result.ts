export class Result {
  status: number = 0;
  code: number = 0;
  message: string = '';
}