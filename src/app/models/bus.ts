export class Bus {
  id: string = '';
  tenantId: string = '';
  code: string = '';
  licensePlate: string = '';
  operatorCode: string = '';
  type: string = '';
  wheelchair: boolean = false;
  direction: number = 1;
  routeCode: string = '';
  originStopCode: string = '';
  destStopCode: string = '';
  amPeakFreq: string = '';
  amOffpeakFreq: string = '';
  pmPeakFreq: string = '';
  pmOffpeakFreq: string = '';
  lastUpdate: string = '';
  updatedBy: string = '';
  avatar: string = '';

  imageUrl: string = ''; //for ui display purpose
}

export class BusFilter {
  tenantId: string = '';
  pageIndex: number = 0;
  pageSize: number = 20;
  sortBy: string = 'code';
  ascSort: boolean = true;
  keyword: string = '';
}