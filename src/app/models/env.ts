export interface Env {
  applicationName: string,
  versionNumber: string;
  bimsApiEndpoint: string,
  identityApiEndpoint: string,
  identityLoginUrl: string,
  enableGreet: boolean;
  timezoneUtc: string[];
};
