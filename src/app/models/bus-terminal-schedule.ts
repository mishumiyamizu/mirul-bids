export class BusTerminalSchedule {
  id: string = '';
  tenantId: string = '';
  operatorCode: string = '';
  tripCode: string = '';
  gate: string = '';
  bay: string = '';
  plateNo: string = '';
  departureTime: string = '';
  destination: string = '';
  status: string = '';
  origin: string = '';
  arrivalTime: string = '';
  type: string = '';
  totalSeat: number = 0;
  date: string = '';
}

export class BusTerminalScheduleFilter {
  tenantId: string = '';
  pageIndex: number = 0;
  pageSize: number = 20;
  sortBy: string = 'departureTime';
  ascSort: boolean = true;
  keyword: string = '';
  origin: string = '';
  destination: string = '';
  departureTime: string = '';
}