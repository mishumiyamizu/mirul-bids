export class BusStop {
  id: string = '';
  tenantId: string = '';
  code: string = '';
  name: string = '';
  latitude: number = 3.1342771247168257;
  longitude: number = 101.68605635108257;
  roadName: string = ''
  landmark: string = '';
  status: string = '';
  country: string = '';
  city: string = '';
  timezoneUTC: string = '';
  lastUpdate: string = '';
  updatedBy: string = '';
  avatar: string = '';

  imageUrl: string = ''; //for ui display purpose
}

export class BusStopFilter {
  tenantId: string = '';
  pageIndex: number = 0;
  pageSize: number = 20;
  sortBy: string = 'code';
  ascSort: boolean = true;
  keyword: string = '';
}