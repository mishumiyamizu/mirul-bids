import { BusStop } from "./bus-stop";

export class BusRoute {
  id: string = '';
  tenantId: string = '';
  code: string = '';
  name: string = '';
  operatorCode: string = '';
  totalBusStop: number = 0;
  originBusStop!: BusStopData;
  destBusStop!: BusStopData;
  directionToOrigin: BusStopData[] = [];
  directionToDest: BusStopData[] = [];
  lastUpdate: string = '';
  updatedBy: string = '';
}

export class BusRouteFilter {
  tenantId: string = '';
  pageIndex: number = 0;
  pageSize: number = 20;
  sortBy: string = 'code';
  ascSort: boolean = true;
  keyword: string = '';
  code: string = '';
}

export interface BusStopData {
  code: string;
  name: string;
  busStopSequence: number;
  distance: number;
  wdFirstBus: string;
  wdLastBus: string;
  satFirstBus: string;
  satLastBus: string;
  sunFirstBus: string;
  sunLastBus: string;
}