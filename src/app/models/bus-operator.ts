export class BusOperator {
  id: string = '';
  tenantId: string = '';
  code: string = '';
  name: string = '';
  abbrName: string = '';
  lastUpdate: string = '';
  updatedBy: string = '';
  avatar: string = '';

  imageUrl: string = ''; //for ui display purpose
}

export class BusOperatorFilter {
  tenantId: string = '';
  pageIndex: number = 0;
  pageSize: number = 20;
  sortBy: string = 'code';
  ascSort: boolean = true;
  keyword: string = '';
}